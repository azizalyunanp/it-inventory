<?php

ini_set('max_execution_time','50000');
ini_set('memory_limit','-1');
ini_set('display_errors','on');

session_start();
ob_start();
include('../../inc/inc.koneksi.php');

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

$subid	= $_GET['subid'];
$tgl_awal=$_GET['tgl_awal'];
$tgl_akhir=$_GET['tgl_akhir'];
$sql = mysql_query("SELECT a.*,b.id_jenis,b.nm_jenis as jenis_pekerjaan, c.kode_barang,c.nama_barang,c.stok_awal,c.stok_masuk,c.stok_keluar,c.stok_akhir,c.penyesuaian,c.opname,c.selisih
				FROM mutasi_hasil as a 
				JOIN jenis_barang as b 
				ON (a.jenis_pekerjaan=b.id_jenis)
				JOIN barang as c
				ON (a.kode_barang=c.kode_barang)
				WHERE tgl_hasil BETWEEN '$tgl_awal' AND '$tgl_akhir' GROUP BY a.kode_barang ASC");
$num_rows=mysql_num_rows($sql);
?>
<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Laporan Pertanggungjawaban Mutasi Barang Jadi</title>
<link rel="stylesheet" type="text/css" href="../../mycss/laporan.css" />
</head>
<body>
<div id="logo">
<img src="../../mycss/images/logo2.png" width="100%" height="100%">
<!---<strong>PT SARI WARNA ASLI<br/>
Unit Garment</strong><br/>
Website: http://www.swagarment.com---->
</div>
<div id="title">
 <div align='center'>Laporan Pertanggungjawaban Mutasi Barang Jadi</div>
 <br/>
 <br/>
 Periode : <?php 
 		$tahun = substr($tgl_awal, 0, 4); // memisahkan format tahun menggunakan substring
        $bulan = substr($tgl_awal, 5, 2); // memisahkan format bulan menggunakan substring
        $tgl   = substr($tgl_awal, 8, 2); // memisahkan format tanggal menggunakan substring
		$tahunakhir = substr($tgl_akhir, 0, 4); // memisahkan format tahun menggunakan substring
        $bulanakhir = substr($tgl_akhir, 5, 2); // memisahkan format bulan menggunakan substring
        $tglakhir   = substr($tgl_akhir, 8, 2); // memisahkan format tanggal menggunakan substring
		
		$startDate = date('Y-01-00');
		$endDate = date('Y-m-d H:i:s');
        
        $resultawal = $tgl . "-" . $bulan . "-". $tahun;
		$resultakhir = $tglakhir . "-" . $bulanakhir . "-". $tahunakhir;
 
 echo "$resultawal Sampai $resultakhir"; ?><p></p>
</div>
  <div id="isi">
  <table width="100%" border="0.25" align="left" cellpadding="0" cellspacing="0">
  <tr class="tr-title" align="center">
  	<td>No</td>
    <td width="70">Kode Barang</td>
    <td width="200">Nama Barang</td>
	<td width="30">Sat</td>
    <td width="60">Saldo Awal</td>
	<td width="80">Pemasukan</td>
	<td width="80">Pengeluaran</td>
	<td width="80">Penyesuaian</td>
	<td width="60">Saldo Akhir</td>
	<td width="60">Opname</td>
	<td width="60">Selisih</td>
    <td width="150">Keterangan</td>
  </tr>

  <?php
  
$tahun = substr($tgl_awal, 0, 4);
$rs = mysql_query("SELECT a.*,b.id_jenis,b.nm_jenis as jenis_pekerjaan, c.kode_barang,c.nama_barang,c.stok_awal,c.stok_masuk,c.stok_keluar,c.stok_akhir,c.penyesuaian,c.opname,c.selisih
				FROM mutasi_hasil as a 
				JOIN jenis_barang as b 
				ON (a.jenis_pekerjaan=b.id_jenis)
				JOIN barang as c
				ON (a.kode_barang=c.kode_barang)
				GROUP BY a.kode_barang ASC"); 
	
			$no=1;	
		while($r_data=mysql_fetch_array($rs)){		
			$kode=$r_data['kode_barang'];
			/* echo $kode; */
			$sql=mysql_query("SELECT *,sum(qty) as stok_masuk from mutasi_hasil where tgl_hasil BETWEEN '$tgl_awal' AND '$tgl_akhir' and kode_barang=$kode ");
			$num_rows=mysql_num_rows($sql); 
			
			$sql2=mysql_query("SELECT *,sum(qty) as stok_keluar from barang_keluar where tgl_bukti BETWEEN '$tgl_awal' AND '$tgl_akhir' and kode_barang=$kode ");
			$num_rows2=mysql_num_rows($sql2); 
			
			/* $sql3=mysql_query("SELECT *,sum(qty) as stok_awal from mutasi_hasil where tgl_hasil>'$startDate' and tgl_hasil<'$tgl_awal' and kode_barang=$kode "); */
			$sql3=mysql_query("SELECT *,sum(qty) as stok_awal from mutasi_hasil where tgl_hasil<'$tgl_awal' and kode_barang=$kode ");
			$num_rows3=mysql_num_rows($sql3); 
			
			/* $sql4=mysql_query("SELECT *,sum(qty) as stok_keluar_tot from barang_keluar where tgl_bukti>'$startDate' and tgl_bukti<'$tgl_awal' and kode_barang=$kode "); */
			$sql4=mysql_query("SELECT *,sum(qty) as stok_keluar_tot from barang_keluar where tgl_bukti<'$tgl_awal' and kode_barang=$kode ");
			$num_rows4=mysql_num_rows($sql4); 
			
			$sql5=mysql_query("SELECT keterangan from barang_keluar where tgl_bukti>'$startDate' and tgl_bukti<'$tgl_akhir' and kode_barang=$kode group by kode_barang DESC");
			$num_rows5=mysql_num_rows($sql5); 

	$total_harga=0;
	$total_barang=0;
	/* for($i=1; $i<=$num_rows; $i++){ */
	$rows=mysql_fetch_array($sql);
	$rows2=mysql_fetch_array($sql2);
	$rows3=mysql_fetch_array($sql3);
	$rows4=mysql_fetch_array($sql4);
	$rows5=mysql_fetch_array($sql5);
	$kd_barang=$r_data['kode_barang'];
	$nm_barang=$r_data['nama_barang'];
	$satuan=$r_data['satuan'];
	$saldoawal=$rows3['stok_awal']-$rows4['stok_keluar_tot'];
	$saldoawal= number_format("$saldoawal",2,".","");
	$pemasukan=$rows['stok_masuk'];
	$pengeluaran=$rows2['stok_keluar'];
	$penyesuaian=$rows['penyesuaian'];
	/* $saldoakhir=($rows3['stok_awal']-$rows4['stok_keluar_tot'])+$rows['stok_masuk']-$rows2['stok_keluar']; */
	$saldoakhir=$saldoawal+$rows['stok_masuk']-$rows2['stok_keluar'];
	$opname=$rows['opname'];
	$selisih=$rows['selisih'];
	$keterangan=$rows5['keterangan'];

/* 	$total_harga=0;
	$total_barang=0;
	for($i=1; $i<=$num_rows; $i++){
	$rows=mysql_fetch_array($sql);
	$kd_barang=$rows['kode_barang'];
	$nm_barang=$rows['nama_barang'];
	$satuan=$rows['satuan'];
	$saldoawal=$rows['stok_awal'];
	$pemasukan=$rows['stok_masuk'];
	$pengeluaran=$rows['stok_keluar'];
	$penyesuaian=$rows['penyesuaian'];
	$saldoakhir=$rows['stok_akhir'];
	$opname=$rows['opname'];
	$selisih=$rows['selisih'];
	$keterangan=$rows['keterangan']; */
	
	$total_barang=$jml_masuk+$total_barang;
	$total_harga=$sub_total+$total_harga;
	echo"	<tr align='center'>
			<td>$no</td>
			<td>$kd_barang</td>
			<td width='100'>$nm_barang</td>
			<td>$satuan</td>
			<td>$saldoawal</td>
			<td>$pemasukan</td>
			<td>$pengeluaran</td>
			<td>$penyesuaian</td>
			<td>$saldoakhir</td>
			<td>$opname</td>
			<td>$selisih</td>
			<td>$keterangan</td>

		</tr>";
		$no++;
	}
	// echo "<tr>
    // <td colspan='7' align='right'>TOTAL</td>
    // <td>$total_barang</td>
    // <td>$total_harga</td>
  // </tr>";
 ?>
  
</table>
<br/>
<br/>
<div>
<?
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo "<p><font size='2' face='Arial'>Optimasi data selesai dalam ".$total_time. "</font></p>";
?>
</div>
</div>

</body>
</html><!-- Akhir halaman HTML yang akan di konvert -->
<?php
$filename="Laporan Mutasi Barang Jadi.pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
$content = ob_get_clean();
	$content = '<page style="font-family: freeserif">'.($content).'</page>';
	require_once('../../html2pdf_v4.03/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(20, 10, 10, 10));
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>