<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

session_start();
ob_start();
include('../../inc/inc.koneksi.php');
$subid	= $_GET['subid'];
$tgl_awal=$_GET['tgl_awal'];
$tgl_akhir=$_GET['tgl_akhir'];
$sql = mysql_query("SELECT a.*,b.kode,b.nama as id_supplier FROM data_order as a JOIN customer as b ON (a.id_supplier=b.kode) WHERE tgl_order BETWEEN '$tgl_awal' AND '$tgl_akhir' ORDER BY no_kontrak ASC");
$num_rows=mysql_num_rows($sql);
?>
<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Laporan Barang Proses</title>
<link rel="stylesheet" type="text/css" href="../../mycss/laporan.css" />
</head>
<body>
<div id="logo">
<img src="../../mycss/images/logo2.png" width="100%" height="100%">
<!---<strong>PT SARI WARNA ASLI<br/>
Unit Garment</strong><br/>
Website: http://www.swagarment.com---->
</div>
<div id="title">
 <div align='center'>Laporan Pemasukan Barang per Dokumen Pabean</div>
 <br/>
 <br/>
 Periode : <?php 
 		$tahun = substr($tgl_awal, 0, 4); // memisahkan format tahun menggunakan substring
        $bulan = substr($tgl_awal, 5, 2); // memisahkan format bulan menggunakan substring
        $tgl   = substr($tgl_awal, 8, 2); // memisahkan format tanggal menggunakan substring
		$tahunakhir = substr($tgl_akhir, 0, 4); // memisahkan format tahun menggunakan substring
        $bulanakhir = substr($tgl_akhir, 5, 2); // memisahkan format bulan menggunakan substring
        $tglakhir   = substr($tgl_akhir, 8, 2); // memisahkan format tanggal menggunakan substring
        
        $resultawal = $tgl . "-" . $bulan . "-". $tahun;
		$resultakhir = $tglakhir . "-" . $bulanakhir . "-". $tahunakhir;
 
 echo "$resultawal Sampai $resultakhir"; ?><p></p>
</div>
  <div id="isi">
  <table width="100%" border="0.25" align="left" cellpadding="0" cellspacing="0">
  <tr class="tr-title" align="center">
  	<td>No</td>
    <td>Jenis<br/>Dokumen</td>
    <td>No.Dok <br/>Pabean</td>
	<td>Tgl.Dok <br/>Pabean</td>
    <td>No. Bukti<br/>Penerimaan Barang</td>
	<td>Tgl. Bukti<br/>Penerimaan Barang</td>
	<td>Pemasok/Pengirim</td>
    <td>Kode Barang</td>
    <td>Nama Barang</td>
	<td>Sat</td>
    <td>Jumlah</td>
    <td>Nilai Barang</td>
  </tr>
<?php
	$total_harga=0;
	$total_barang=0;
	for($i=1; $i<=$num_rows; $i++){
	$rows=mysql_fetch_array($sql);
	$id_masuk=$rows['id_order'];
	$tgl_masuk=$rows['tgl_order'];
	$no_kontrak=$rows['no_kontrak'];
	$supplier=$rows['id_supplier'];
	$id_barang=$rows['nama_barang'];
	$jml_masuk=$rows['qty'];
	$hrg_beli=$rows['harga'];
	$sub_total=$rows['sub_total'];
	$total_barang=$jml_masuk+$total_barang;
	$total_harga=$sub_total+$total_harga;
	echo"	<tr>
			<td>$i</td>
			<td>$id_masuk</td>
			<td>$tgl_masuk</td>
			<td>$no_kontrak</td>
			<td width='30'>$supplier</td>
			<td>$id_barang</td>
			<td>$hrg_beli</td>
			<td>$jml_masuk</td>
			<td>$sub_total</td>
		</tr>";
	}
	echo "<tr>
    <td colspan='7' align='right'>TOTAL</td>
    <td>$total_barang</td>
    <td>$total_harga</td>
  </tr>";
 ?>
  
</table>
</div>


</body>
</html><!-- Akhir halaman HTML yang akan di konvert -->
<?php
$filename="Laporan Barang Masuk.pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
$content = ob_get_clean();
	$content = '<page style="font-family: freeserif">'.($content).'</page>';
	require_once('../../html2pdf_v4.03/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(20, 10, 10, 10));
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>