<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Barang Keluar</title>

<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/style.css" />
<script type="text/javascript" src="../../jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="../../jquery_easyui/jquery.easyui.min.js"></script>

</head>
<?php
include('../../inc/inc.koneksi.php');?>
<body>
<br clear="all" />
<div id="menu">
	<ul><li><a href="pilih_lap.php" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Berdasarkan Tanggal','laporan/mutasi_proses/pilih_lap.php')">Laporan Berdasarkan Tanggal</a><br /></li></ul>
	<ul><li><a href="pilih_lap2.php" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Berdasarkan Kategori','laporan/mutasi_proses/pilih_lap2.php')">Laporan Berdasarkan Kategori</a><br /></li></ul>
	
</div>
</body>
</html>
