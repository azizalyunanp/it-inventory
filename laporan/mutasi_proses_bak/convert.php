<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

session_start();
ob_start();
include('../../inc/inc.koneksi.php');

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;


$subid	= $_GET['subid'];
$tgl_awal=$_GET['tgl_awal'];
$tgl_akhir=$_GET['tgl_akhir'];
?>
<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Laporan Pertanggungjawaban  Mutasi Bahan Baku / Bahan Penolong</title>
<link rel="stylesheet" type="text/css" href="../../mycss/laporan.css" />
</head>
<body>
<div id="logo">
<img src="../../mycss/images/logo2.png" width="100%" height="100%">
<!---<strong>PT SARI WARNA ASLI<br/>
Unit Garment</strong><br/>
Website: http://www.swagarment.com---->
</div>
<div id="title">
 <div align='center'>Laporan Pertanggungjawaban  Mutasi Bahan Baku / Bahan Penolong</div>
 <br/>
 <br/>
 Periode : <?php 
 		$tahun = substr($tgl_awal, 0, 4); // memisahkan format tahun menggunakan substring
        $bulan = substr($tgl_awal, 5, 2); // memisahkan format bulan menggunakan substring
        $tgl   = substr($tgl_awal, 8, 2); // memisahkan format tanggal menggunakan substring
		$tahunakhir = substr($tgl_akhir, 0, 4); // memisahkan format tahun menggunakan substring
        $bulanakhir = substr($tgl_akhir, 5, 2); // memisahkan format bulan menggunakan substring
        $tglakhir   = substr($tgl_akhir, 8, 2); // memisahkan format tanggal menggunakan substring
        
        $resultawal = $tgl . "-" . $bulan . "-". $tahun;
		$resultakhir = $tglakhir . "-" . $bulanakhir . "-". $tahunakhir;
 
 echo "$resultawal Sampai $resultakhir"; ?><p></p>
</div>
  <div id="isi">
  <table width="100%" border="0.25" align="left" cellpadding="0" cellspacing="0">
  <tr class="tr-title" align="center">
  	<td>No</td>
    <td width="70">Kode Barang</td>
    <td width="200">Nama Barang</td>
	<td width="30">Satuan</td>
    <td width="60">Saldo Awal</td>
	<td width="80">Pemasukan</td>
	<td width="80">Pengeluaran</td>
	<td width="80">Penyesuaian</td>
	<td width="60">Saldo Akhir</td>
	<td width="60">Opname</td>
	<td width="60">Selisih</td>
    <td width="150">Keterangan</td>
  </tr>
<?php

$rs = mysql_query("SELECT * from temp1 order by kode_barang ASC"); 
	
		$no=1;	
		while($r_data=mysql_fetch_array($rs)){		
			$kd_barang=$r_data['kode_barang'];
			$nm_barang=$r_data['nama_barang'];
			$satuan=$r_data['satuan'];
			$saldo_awal=$r_data['saldo_awal'];
			$pemasukan=$r_data['pemasukan'];
			$pengeluaran=$r_data['pengeluaran'];
			$penyesuaian=$r_data['penyesuaian'];
			$saldo_akhir=$r_data['saldo_akhir'];
			$opname=$r_data['opname'];
			$selisih=$r_data['selisih'];
			$keterangan=$r_data['keterangan'];

	echo"	<tr align='center'>
			<td>$no</td>
			<td>$kd_barang</td>
			<td width='100'>$nm_barang</td>
			<td>$satuan</td>
			<td>$saldoawal</td>
			<td>$pemasukan</td>
			<td>$pengeluaran</td>
			<td>$penyesuaian</td>
			<td>$saldoakhir</td>
			<td>$opname</td>
			<td>$selisih</td>
			<td>$keterangan</td>

		</tr>";
		$no++;
	}
 ?>
  
</table>
<br/>
<br/>
<div>
<?
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo "<p><font size='2' face='Arial'>Optimasi data selesai dalam ".$total_time."</font></p>";
?>
</div>
</div>
</body>
</html><!-- Akhir halaman HTML yang akan di konvert -->
<?php
$filename="LaporanMutasiBahanBakudanPenolong.pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
$content = ob_get_clean();
	$content = '<page style="font-family: freeserif">'.($content).'</page>';
	require_once('../../html2pdf_v4.03/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(20, 10, 10, 10));
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>