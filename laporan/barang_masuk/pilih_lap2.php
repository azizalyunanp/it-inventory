<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Barang Masuk</title>

<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/style.css" />
<script type="text/javascript" src="../../jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="../../jquery_easyui/jquery.easyui.min.js"></script>
<script type="text/javascript">
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}

</script>
<script type="text/javascript">

$(document).ready(function() {

	$('#loader').hide();
	$('#show_heading').hide();
	
	$('#search_category_id').change(function(){
		$('#show_sub_categories').fadeOut();
		$('#loader').show();
		$.post("get_chid_categories.php", {
			parent_id: $('#search_category_id').val(),
		}, function(response){
			
			setTimeout("finishAjax('show_sub_categories', '"+escape(response)+"')", 400);
		});
		return false;
	});
});

function finishAjax(id, response){
  $('#loader').hide();
  $('#show_heading').show();
  $('#'+id).html(unescape(response));
  $('#'+id).fadeIn();
} 

function alert_id()
{
	if($('#sub_category_id').val() == '')
	alert('Please select a sub category.');
	else
	alert($('#sub_category_id').val());
	return false;
}
</script>



</head>
<?php
include('../../inc/inc.koneksi.php');?>
<body>
<h2>LAPORAN BARANG MASUK</h2>
<div class="info" style="margin-bottom:10px">
		<div class="tip icon-tip">&nbsp;</div>
		<div>Pilih Berdasar Kategori.</div>
</div>
    
<div style="margin:10px 0;">
<form action="pilih_lap2.php" id="fmFilter" method="post" novalidate>
Pilih Kategori: 
		<select name="search_category" id="search_category_id" class="input">
		<option value="" selected="selected"></option>
	     <option value="12">No Kontrak</option>
         <option value="34">Kode Barang</option>
         <option  value="56">Customer</option>
		</select>	
		<br clear="all" /><br clear="all" />
Sub Kategori: 
		<div id="show_sub_categories" align="" style="">
			<img src="loader.gif" style="margin-top:8px; float:left" id="loader" alt="" />
		</div>
		
		<br clear="all" />
<input type="submit" name="button" id="button" value="Go" style=""/>


</form>
</div>


<div id="tampil_lap" style="margin-top:30px; margin-bottom:20px;">
<table id="dg" title="DATA BARANG MASUK" class="easyui-datagrid" style="height:auto;"
			url="tampil_lap2.php?tgl_awal=<?php echo $_REQUEST['tgl_awal'] ?>&tgl_akhir=<?php echo $_REQUEST['tgl_akhir']; ?>&sid=<?php echo $_REQUEST['search_category']; ?>&subid=<?php echo $_REQUEST['sub_category_id']; ?>";
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
			  <th field="no_bukti" width="50">ID TRANSAKASI</th>
			  <th field="tgl_bukti" width="50">TANGGAL</th>
              <th field="id_supplier" width="50">SUPPLIER</th>
              <th field="nama_barang" width="50">ID BARANG</th>
              <th field="qty" width="50">JML MASUK</th>
              <th field="harga" width="50">HRG BELI</th>
              <th field="sub_total" width="50">SUB TOTAL</th>
			</tr>
		</thead>
  </table> 
  </div> 
  
  
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-pdf" plain="true" 
onclick="window.open('convert2.php?tgl_awal=<?php echo $_POST['tgl_awal']; ?>
&tgl_akhir=<?php echo $_POST['tgl_akhir']; ?>&sid=<?php echo $_REQUEST['search_category']; ?>&subid=<?php echo $_REQUEST['sub_category_id']; ?>
','Laporan Barang Masuk','size=800,height=800,scrollbars=yes,resizeable=no')">PDF</a>

</body>
</html>
