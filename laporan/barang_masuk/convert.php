<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

session_start();
ob_start();
include('../../inc/inc.koneksi.php');

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;


$subid	= $_GET['subid'];
$tgl_awal=$_GET['tgl_awal'];
$tgl_akhir=$_GET['tgl_akhir'];
$sql = mysql_query("SELECT a.no_bukti,a.tgl_bukti,a.no_pabean,a.satuan,a.cur,a.qty,a.harga,b.kode,b.nama as id_supplier,c.id_jenisdok,c.nm_jenisdok as id_jenisdok FROM barang_masuk as a JOIN customer as b ON (a.id_supplier=b.kode) JOIN jenis_dokumen as c ON (a.id_jenisdok=c.id_jenisdok) WHERE tgl_bukti BETWEEN '$tgl_awal' AND '$tgl_akhir' ORDER BY no_pabean ASC");
$num_rows=mysql_num_rows($sql);
?>
<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Laporan Pemasukan Barang per Dokumen Pabean</title>
<link rel="stylesheet" type="text/css" href="../../mycss/laporan.css" />
<script type="text/javascript" href="../../printThis.js"></script>
</head>
<body>
<div id="logo">
<img src="../../mycss/images/logo2.png" width="100%" height="100%">
<!---<strong>PT SARI WARNA ASLI<br/>
Unit Garment</strong><br/>
Website: http://www.swagarment.com---->
</div>
<div id="title">
 <div align='center'>Laporan Pemasukan Barang per Dokumen Pabean</div>
 <br/>
 <br/>
 Periode : <?php 
 		$tahun = substr($tgl_awal, 0, 4); // memisahkan format tahun menggunakan substring
        $bulan = substr($tgl_awal, 5, 2); // memisahkan format bulan menggunakan substring
        $tgl   = substr($tgl_awal, 8, 2); // memisahkan format tanggal menggunakan substring
		$tahunakhir = substr($tgl_akhir, 0, 4); // memisahkan format tahun menggunakan substring
        $bulanakhir = substr($tgl_akhir, 5, 2); // memisahkan format bulan menggunakan substring
        $tglakhir   = substr($tgl_akhir, 8, 2); // memisahkan format tanggal menggunakan substring
        
        $resultawal = $tgl . "-" . $bulan . "-". $tahun;
		$resultakhir = $tglakhir . "-" . $bulanakhir . "-". $tahunakhir;
 
 echo "$resultawal Sampai $resultakhir"; ?><p></p>
</div>

  <div id="isi">
  <table width="100%" border="0.25" align="left" cellpadding="0" cellspacing="0">
  <tr class="tr-title" align="center">
  	<td>No</td>
    <td width="60">Jenis<br/>Dokumen</td>
    <td width="60">No.Dok <br/>Pabean</td>
	<td width="60">Tgl.Dok <br/>Pabean</td>
    <td width="90">No. Bukti<br/>Penerimaan Barang</td>
	<td width="70">Tgl. Bukti<br/>Penerimaan Barang</td>
	<td width="120">Pemasok/Pengirim</td>
    <td width="70">Kode Barang</td>
    <td width="120">Nama Barang</td>
	<td width="30">Sat</td>
    <td width="40">Jumlah</td>
	<td width="20">Cur</td>
    <td width="90">Nilai Barang</td>
  </tr>
<?php
	$total_harga=0;
	$total_barang=0;
	for($i=1; $i<=$num_rows; $i++){
	$rows=mysql_fetch_array($sql);
	$id_jenisdok=$rows['id_jenisdok'];
	$no_pabean=$rows['no_pabean'];
	$tgl_pabean=$rows['tgl_pabean'];
	$thn = substr($tgl_pabean, 0, 4); // memisahkan format tahun menggunakan substring
    $bln = substr($tgl_pabean, 5, 2); // memisahkan format bulan menggunakan substring
    $tg   = substr($tgl_pabean, 8, 2); // memisahkan format tanggal menggunakan substring
	$tgl_pabean2 = $tg . "-" . $bln . "-". $thn;
	$no_bukti=$rows['no_bukti'];
	$tgl_bukti=$rows['tgl_bukti'];
	$thnx = substr($tgl_bukti, 0, 4); // memisahkan format tahun menggunakan substring
    $blnx = substr($tgl_bukti, 5, 2); // memisahkan format bulan menggunakan substring
    $tgx   = substr($tgl_bukti, 8, 2); // memisahkan format tanggal menggunakan substring
	$tgl_bukti2 = $tgx . "-" . $blnx . "-". $thnx;
	$supplier=$rows['id_supplier'];
	$kd_barang=$rows['kode_barang'];
	$nm_barang=$rows['nama_barang'];
	$satuan=$rows['satuan'];
	$cur=$rows['cur'];
	$jml_masuk=$rows['qty'];
	$hrg_beli=$rows['harga'];
	$total=$rows['total'];
	$total_barang=$jml_masuk+$total_barang;
	$total_harga=$sub_total+$total_harga;
	echo"	<tr align='center'>
			<td>$i</td>
			<td>$id_jenisdok</td>
			<td>$no_pabean</td>
			<td>$tgl_pabean2</td>
			<td>$no_bukti</td>
			<td>$tgl_bukti2</td>
			<td width='50'>$supplier</td>
			<td>$kd_barang</td>
			<td width='30'>$nm_barang</td>
			<td>$satuan</td>
			<td align='right'>$jml_masuk</td>
			<td>$cur</td>
			<td align='right'>$total</td>
		</tr>";
	}
	// echo "<tr>
    // <td colspan='7' align='right'>TOTAL</td>
    // <td>$total_barang</td>
    // <td>$total_harga</td>
  // </tr>";
 ?>
  
</table>
<br/>
<br/>
<div>
<?
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo "<p><font size='2' face='Arial'>Optimasi data selesai dalam ".$total_time. "</font></p>";
?>
</div>
</div>


</body>
</html><!-- Akhir halaman HTML yang akan di konvert -->
<?php
$filename="Laporan Barang Masuk.pdf"; //ubah untuk menentukan nama file pdf yang dihasilkan nantinya
//==========================================================================================================
$content = ob_get_clean();
	$content = '<page style="font-family: freeserif">'.($content).'</page>';
	require_once('../../html2pdf_v4.03/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('L','A4','en', False, 'ISO-8859-15',array(20, 10, 10, 10));
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($filename);
	}
	catch(HTML2PDF_exception $e) { echo $e; }
?>