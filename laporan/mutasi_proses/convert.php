<?php
// MUTASI BAHAN BAKU DAN PENOLONG
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

session_start();
ob_start();
include('../../inc/inc.koneksi.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml"> <!-- Bagian halaman HTML yang akan konvert -->
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
	<!--DATATABLES-->
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<!--DATATABLES-->
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
	 	<script type="text/javascript" src="../../js/printThis.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
 

<!-- DATATABLES -->
</head>
<body>
<div class="pull-right">
<br>
	<button type="button" class="btn btn-primary" onclick="$('#printsaja').printThis()"><span class="glyphicon glyphicon-print"></span> Print</button>
</div>
<div id="printsaja">
	<div id="logo">
	<img src="../../mycss/images/logo2.png" width="700" class="img-responsive">
	<!---<strong>PT SARI WARNA ASLI<br/>
	Unit Garment</strong><br/>
	Website: http://www.swagarment.com-->
	</div>

	<div id="title">
	 <div align='center'>Laporan Pertanggungjawaban  Mutasi Bahan Baku / Bahan Penolong</div>
	 <br/>
	 <br/>
	 Periode : <?php 
	 		$tahun = substr($_GET['tgl_awal'], 0, 4); // memisahkan format tahun menggunakan substring
	        $bulan = substr($_GET['tgl_awal'], 5, 2); // memisahkan format bulan menggunakan substring
	        $tgl   = substr($_GET['tgl_awal'], 8, 2); // memisahkan format tanggal menggunakan substring
			$tahunakhir = substr($_GET['tgl_akhir'], 0, 4); // memisahkan format tahun menggunakan substring
	        $bulanakhir = substr($_GET['tgl_akhir'], 5, 2); // memisahkan format bulan menggunakan substring
	        $tglakhir   = substr($_GET['tgl_akhir'], 8, 2); // memisahkan format tanggal menggunakan substring
	        
	        $resultawal = $tgl . "-" . $bulan . "-". $tahun;
			$resultakhir = $tglakhir . "-" . $bulanakhir . "-". $tahunakhir;
	 
	 echo "$resultawal Sampai $resultakhir"; ?><p></p>
	</div>
	<table id="datatable_example" class="display" width="100%" cellspacing="0">
	<thead>
	<tr>
	    <th>KODE BARANG</th>
		<th>NAMA BARANG</th>
		<th>SATUAN</th>
		<th>SALDO AWAL</th>
		<th>PEMASUKAN</th>
		<th>PENGELUARAN</th>
		<th>PENYESUAIAN</th>
		<th>SALDO AKHIR</th>
		<th>OPNAME</th>
		<th>SELISIH</th>
		<th>KETERANGAN</th>
	</tr>
	</thead>
	</table>
</div>
<script type="text/javascript">
	$( document ).ready(function() {
	var table = $('#datatable_example').dataTable({
		dom: 'Bfrtip',
		"bProcessing": true,
		"sAjaxSource": "tampil_lap.php?tgl_awal=<?php echo $_GET[tgl_awal]; ?>&tgl_akhir=<?php echo $_GET[tgl_akhir]; ?>",
		"bPaginate":false,
		"bFilter" : false,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 5,
		"aoColumns": [
		{ mData: 'kode_barang' } ,
		{ mData: 'nama_barang' },
		{ mData: 'satuan' },
		{ mData: 'saldo_awal' },
		{ mData: 'pemasukan' },
		{ mData: 'pengeluaran' },
		{ mData: 'penyesuaian' },
		{ mData: 'saldo_akhir' },
		{ mData: 'opname' },
		{ mData: 'selisih' },
		{ mData: 'keterangan' },
		]
	});
});
</script>

</body>
</html>

