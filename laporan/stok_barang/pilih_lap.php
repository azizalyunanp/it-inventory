<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Laporan Barang Keluar</title>

<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/style.css" />
<script type="text/javascript" src="../../jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="../../jquery_easyui/jquery.easyui.min.js"></script>

</head>

<body>
<h2>LAPORAN STOK BARANG</h2>


<div id="tampil_lap">
<table id="dg" title="DATA STOK BARANG" class="easyui-datagrid" style="height:auto;"
url="tampil_lap.php"; rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
			  <th field="kode_barang" width="30">ID BARANG</th>
			  <th field="nama_barang" width="70">NAMA BARANG</th>
              <th field="kelompok" width="40">KELOMPOK</th>
              <th field="satuan" width="20">SATUAN</th>
			  <th field="keterangan" width="70">KETERANGAN</th>
              <th field="stok_awal" width="30">STOK AWAL</th>
              <th field="stok_masuk" width="30">STOK MASUK</th>
			  <th field="stok_keluar" width="30">STOK KELUAR</th>
              <th field="stok_akhir" width="30">STOK AKHIR</th>
			</tr>
		</thead>
  </table>
  <br />
  <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-pdf" plain="true" onclick="window.open('convert.php','Laporan Stok Barang','size=800,height=800,scrollbars=yes,resizeable=no')">PDF</a>
</div>
</body>
</html>
