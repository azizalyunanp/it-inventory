<?php
##PERHITUNGAN PROSES BAHAN BAKU
ini_set('max_execution_time', 0);
ob_start();
header('Content-Type: application/json');
include('../../koneksi/koneksi.php');
$tgl_awal  		= $_GET['tgl_awal'];
$tgl_akhir  	= $_GET['tgl_akhir'];
$kode_barang 	= $_GET['kode_barang'];

if(!$tgl_awal == "") {
 
############################################### SALDO AWAL #######################################
$rs  	= mysql_query("SELECT kode_barang,nama_barang,satuan from mutasi_proses where jenis_mutasi='JM01' AND tgl_proses BETWEEN '$tgl_awal' AND '$tgl_akhir' AND kode_barang='$kode_barang' group by kode_barang ASC"); 
 	$response = array();
  	$response["data"] = array();

	while($r_data=mysql_fetch_array($rs)){		
	$kode=$r_data['kode_barang'];
 
			$sql=mysql_query("SELECT keterangan,COALESCE(SUM(qty),0) AS stok_awal from barang_masuk where tgl_bukti<'$tgl_awal' and kode_barang='$kode'"); 
			$sql3=mysql_query("SELECT COALESCE(SUM(qty),0) as stok_keluar_tot from mutasi_proses where tgl_proses<'$tgl_awal' and kode_barang='$kode'");

			$rows 		= mysql_fetch_array($sql);
			$rows3		= mysql_fetch_array($sql3);
				
			$kd_barang 	= $r_data['kode_barang'];
			$nm_barang 	= $r_data['nama_barang'];
			$satuan 	= $r_data['satuan'];
			$saldoawal 	= $rows['stok_awal'] - $rows3['stok_keluar_tot'];


			$h['no_bukti'] 	 		= '-';
    		$h['transaksi'] 		= 'SALDO AWAL';
    		$h['no_kontrak'] 		= $no_kontrak;
    		$h['tanggal_proses'] 	= $tanggal_proses;
    		$h['kode_barang'] 		= $kode_barang;
    		$h['nama_barang'] 		= $nama_barang;
    		$h['satuan'] 			= $satuan;
    		$h['saldo_awal'] 		= str_replace('.', '', number_format($saldoawal,4,",","."));
    		$h['pemasukan'] 		= "0";
    		$h['pengeluaran'] 		= "0";
    		$h['saldo_akhir'] 		= "0";
    		array_push($response["data"], $h);

	

	} 	 
####################################### BARANG MASUK ###################################
	$bm 	= mysql_query("SELECT no_bukti,no_kontrak,qty,tgl_bukti  from barang_masuk where tgl_bukti BETWEEN '$tgl_awal' AND '$tgl_akhir' and kode_barang='$kode_barang'");
	while($data = mysql_fetch_array($bm)) {
		$h['no_bukti'] 	 		= $data['no_bukti'];
		$h['transaksi'] 		= 'BARANG MASUK';
		$h['no_kontrak'] 		= $data['no_kontrak'];
		$h['tanggal_proses'] 	= $data['tgl_bukti'];
		$h['kode_barang'] 		= $kode_barang;
		$h['nama_barang'] 		= $data['nama_barang'];
		$h['satuan'] 			= $data['satuan'];
		$h['saldo_awal'] 		= "0";
		$h['pemasukan'] 		= str_replace('.', '', number_format($data['qty'],4,",","."));
		$h['pengeluaran'] 		= "0";
		$h['saldo_akhir'] 		= "0";
		array_push($response["data"], $h);
	}

############################################## BARANG KELUAR ###################################
	$mp 	= mysql_query("SELECT id_proses,no_kontrak,qty,tgl_proses from mutasi_proses where tgl_proses BETWEEN '$tgl_awal' AND '$tgl_akhir' and kode_barang='$kode_barang'");

	while($data = mysql_fetch_array($mp)) {
		$h['no_bukti'] 	 		= $data['id_proses'];
		$h['transaksi'] 		= 'MUTASI PROSES';
		$h['no_kontrak'] 		= $data['no_kontrak'];
		$h['tanggal_proses'] 	= $data['tgl_proses'];
		$h['kode_barang'] 		= $kode_barang;
		$h['nama_barang'] 		= $data['nama_barang'];
		$h['satuan'] 			= $data['satuan'];
		$h['saldo_awal'] 		= "0";
		$h['pemasukan'] 		= "0";
		$h['pengeluaran'] 		= str_replace('.', '', number_format($data['qty'],4,",","."));
		$h['saldo_akhir'] 		= "0";
		array_push($response["data"], $h);
	}

	############################################## RETUR / BARANG KELUAR ###################################
	$retur 	= mysql_query("SELECT no_bukti,no_kontrak,qty,tgl_bukti from barang_keluar where tgl_bukti BETWEEN '$tgl_awal' AND '$tgl_akhir' and kode_barang='$kode_barang' AND (tipe='33' OR keterangan LIKE '%retur%' )");

	while($data = mysql_fetch_array($retur)) {
		$h['no_bukti'] 	 		= $data['no_bukti'];
		$h['transaksi'] 		= 'RETUR';
		$h['no_kontrak'] 		= $data['no_kontrak'];
		$h['tanggal_proses'] 	= $data['tgl_bukti'];
		$h['kode_barang'] 		= $kode_barang;
		$h['nama_barang'] 		= $data['nama_barang'];
		$h['satuan'] 			= $data['satuan'];
		$h['saldo_awal'] 		= "0";
		$h['pemasukan'] 		= "0";
		$h['pengeluaran'] 		= str_replace('.', '', number_format($data['qty'],4,",","."));
		$h['saldo_akhir'] 		= "0";
		array_push($response["data"], $h);
	}

	echo json_encode($response);
}
?>