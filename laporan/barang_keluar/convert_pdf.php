<?php
	session_start();
	$tgl_awal 	= $_SESSION['masuk_tgl_awal'];
	$tgl_akhir 	= $_SESSION['masuk_tgl_akhir']; 
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!--DATATABLES-->
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">

	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
	<!--DATATABLES-->
	<script type="text/javascript" src="../../js/printThis.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="pull-right">
<br>
	<button type="button" class="btn btn-primary" onclick="$('#printsaja').printThis()"><span class="glyphicon glyphicon-print"></span> Print</button>
</div>
<div id="printsaja">
	<div id="logo">
	<img src="http://localhost/inventory/mycss/images/logo2.png" width="700" class="img-responsive">
	<!---<strong>PT SARI WARNA ASLI<br/>
	Unit Garment</strong><br/>
	Website: http://www.swagarment.com-->
	</div>

	<div id="title">
	 <div align='center'>Laporan Pengeluaran Barang per Dokumen Pabean</div>
	 <br/>
	 <br/>
	 Periode : <?php 
	 		$tahun = substr($_GET['tgl_awal'], 0, 4); // memisahkan format tahun menggunakan substring
	        $bulan = substr($_GET['tgl_awal'], 5, 2); // memisahkan format bulan menggunakan substring
	        $tgl   = substr($_GET['tgl_awal'], 8, 2); // memisahkan format tanggal menggunakan substring
			$tahunakhir = substr($_GET['tgl_akhir'], 0, 4); // memisahkan format tahun menggunakan substring
	        $bulanakhir = substr($_GET['tgl_akhir'], 5, 2); // memisahkan format bulan menggunakan substring
	        $tglakhir   = substr($_GET['tgl_akhir'], 8, 2); // memisahkan format tanggal menggunakan substring
	        
	        $resultawal = $tgl . "-" . $bulan . "-". $tahun;
			$resultakhir = $tglakhir . "-" . $bulanakhir . "-". $tahunakhir;
	 
	 echo "$resultawal Sampai $resultakhir"; ?><p></p>
	</div>
	<table id="datatable_example" class="display" width="100%" cellspacing="0">
	<thead>
	<tr>
	    <td width="60">Jenis<br/>Dokumen</td>
	    <td width="60">No.Dok <br/>Pabean</td>
		<td width="60">Tgl.Dok <br/>Pabean</td>
	    <td width="90">No. Bukti<br/>Penerimaan Barang</td>
		<td width="70">Tgl. Bukti<br/>Penerimaan Barang</td>
		<td width="120">Pemasok/Pengirim</td>
	    <td width="70">Kode Barang</td>
	    <td width="120">Nama Barang</td>
		<td width="30">Sat</td>
	    <td width="40">Jumlah</td>
		<td width="20">Cur</td>
	    <td width="90">Nilai Barang</td>
	</tr>
	</thead>
	</table>
</div>
<script type="text/javascript">
	$( document ).ready(function() {
	var table = $('#datatable_example').dataTable({
	// dom: 'Bfrtip',
 //        buttons: [
 //            'print'
 //        ],
		"bProcessing": true,
		"sAjaxSource": "pengeluaran.php?tgl_awal=<?php echo $_GET[tgl_awal]; ?>&tgl_akhir=<?php echo $_GET[tgl_akhir]; ?>&jenis_dok=<?=$_GET[jenis_dok];?>",
		"bPaginate":false,
		"bFilter" : false,
		"sPaginationType":"full_numbers",
		"iDisplayLength": 5,
		"aoColumns": [
		{ mData: 'nm_jenisdok' } ,
		{ mData: 'no_pabean' },
		{ mData: 'tgl_pabean' },
		{ mData: 'no_bukti' },
		{ mData: 'tgl_bukti' },
		{ mData: 'id_supplier' },
		{ mData: 'kode_barang' },
		{ mData: 'nama_barang' },
		{ mData: 'satuan' },
		{ mData: 'qty' },
		{ mData: 'cur' },
		{ mData: 'nilai' }
		]
	});
});
</script>

</body>
</html>

