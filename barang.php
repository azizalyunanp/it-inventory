<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/style.css">
<script type="text/javascript" src="jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="jquery_easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="libs_js/barang.js"></script>
<script type="text/javascript">
						$.getJSON('data_master/barang/get_satuan.php', function(json){
						$('#satuan').html('');
						$.each(json, function(index, row) {
							$('#satuan').append('<option value='+row.nm_satuan+'>'+row.id_satuan+'/'+row.nm_satuan+'</option>');
						});
					});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$("#nama_barang").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#keterangan").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#keterangan").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#nama_barang").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
						  
});
</script>
</head>

<body>
<h2>OLAH DATA BARANG</h2>
<div class="info" style="margin-bottom:10px">
		<div class="tip icon-tip">&nbsp;</div>
		<div>Klik tombol pada datagrid toolbar untuk melakukan perubahan data.</div>
	</div>
	
	<table id="dg" title="DATA BARANG" class="easyui-datagrid" style="height:350px"
			url="data_master/barang/get_barang.php"
			toolbar="#toolbar" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
            	<th field="kode_barang" width="50">ID BARANG</th>
				<th field="nama_barang" width="50">NAMA BARANG</th>
				<th field="nm_jenis" width="50">KELOMPOK</th>
				<th field="satuan" width="50">SATUAN</th>
                <th field="harga_beli" width="50">HARGA BELI</th>
				<th field="harga_jual" width="50">HARGA JUAL</th>
                <th field="stok_awal" width="50">STOK</th>
				 <th field="keterangan" width="50">KETERANGAN</th>
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newData()">Data Baru</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editData()">Edit Data</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeData()">Hapus Data</a>
		<!----	<span>Nama Barang:</span>
			<input id="nama_barang" style="line-height:26px;border:1px solid #ccc">
			<a href="" class="easyui-linkbutton" plain="true" onclick="search()">Search</a>----->
	</div>
    
    <div id="dlg" class="easyui-dialog" style="width:400px;height:450px;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Informasi Barang</div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label>Kode Barang:</label>
			  	<input name="kode_barang" id="kode_barang" class="easyui-validatebox" required="true">
			</div>
		    <div class="fitem">
				<label>Nama Barang:</label>
			  	<input name="nama_barang" id="nama_barang" class="easyui-validatebox" required="true">
			</div>
            <div class="fitem">
			  	<label>Kelompok Barang:</label>
				<select name="id_jenis"  id="id_jenis" >
                <option></option>
		    	</select>
		  	</div>
            <div class="fitem">
			  	<label>Satuan:</label>
				<select name="satuan"  id="satuan" >
                <option></option>
		    	</select>
		  	</div>
            <div class="fitem">
			<label>Harga Beli:</label>
		  	  <input name="harga_beli" class="easyui-numberbox" data-options="min:0,precision:2" required="true" >
			</div>
            <div class="fitem">
			<label>Harga Jual:</label>
		  	  <input name="harga_jual" class="easyui-numberbox" data-options="min:0,precision:2" required="true" >
			</div>
			          <div class="fitem">
			<label>Stok:</label>
		  	  <input name="stok_awal" class="easyui-numberbox" required="true" >
			</div>
			      <div class="fitem">
			<label>Keterangan:</label>
		  	  <input name="keterangan" class="easyui-validatebox" required="false" >
			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveData()">Save</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancel</a>
	</div>
	 <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-pdf" plain="true" onclick="window.open('data_master/barang/convert.php?','convert','size=800,height=800,scrollbars=yes,resizeable=no')">PDF</a>
</body>
</html>