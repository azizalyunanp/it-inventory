<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Change Log</title>
<div id="title" font-size="14"><strong>
PT. SARI WARNA ASLI <br/>
Unit Garment<br/></strong>
JL. Hos Cokroaminoto no. 28<br/>
Website: <a href="http://swagarment.com" target="_blank">http://swagarment.com</a>
<br/>

<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/style.css" />
<script type="text/javascript" src="../../jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="../../jquery_easyui/jquery.easyui.min.js"></script>

</head>
<body>
<br clear="all" />
<strong> CHANGE LOG: </strong>
<div id="log" align="left" margin-left="10">

<div class="titleside">-SBC.SWAG.2018.10.29b (UPDATE MAYOR)
	<ul>
	<li>UPDATE LAPORAN MUTASI BAHAN BAKU ,</li>
	<li>UPDATE LAPORAN MUTASI BARANG JADI ,</li>
	<li>UPDATE LAPORAN MUTASI MESIN & PERALATAN,</li>
	<li>UPDATE LAPORAN MUTASI BARANG SISA & SCRAP,</li>
	<li>UPDATE LAPORAN PEMASUKAN  - FILTER JENIS DOKUMEN , KODE BARANG</li>
	<li>UPDATE LAPORAN PENGELUARAN  - FILTER JENIS DOKUMEN , KODE BARANG</li>
	 
	</ul>
</div>

<div class="titleside">-SBC.SWAG.2018.07.02b
	<ul>
	<li> - Menu backup database untuk admin</li>
	 
	</ul>
</div>

<div class="titleside">-SBC.SWAG.2018.05.13b
	<ul>
	<li> - Fixed laporan mutasi mesin dan peralatan</li>
	 
	</ul>
</div>


<div class="titleside">-SBC.SWAG.2018.02.23b
	<ul>
	<li> - Fixed Log History</li>
	<li> - Fixed pagination log history </li>
	</ul>
</div>
	
	
	<div class="titleside">- SBC.SWAG.2018.01.27b
	<ul>
	<li> - Fixed load laporan mutasi bahan baku dan penolong </li>
	<li> - Fixed load laporan mutasi barang jadi </li>
	<li> - Fixed load laporan mutasi mesin dan peralatan </li>
	<li> - Fixed load laporan mutasi barang dan sisa scrap </li>
	<li> - Fast load data with javascript load </li>
	<li>     - Fixed PDF Print for All Report </li>

	</ul>
	</div>
	
	<div class="titleside">- SBC.SWAG.2016.11.17b
	<ul>
	<li>refresh session idle 10 mnt</li>
	<li>update hak akses di setiap user (trial error)</li>
	<li>laporan menampilkan penyesuaian dan stok opname (trial error)</li>
	<li>Add menu dan form penyusuaian barang dan stok opname (trial error)</li>
	<li>Mengembalikan menu cctv pada panel superadmin/admin</li>
	<li>Mengubah hak akses sesuai klasifikasi tertentu</li>
	<li>Add Log History untuk all user</li>
	<li>Add management user & log user untuk superadmin/admin</li>
	<li>Mengembalikan tampilan fastreport</li>
	<li>Update Version (Beta)</li>
	<li>Update Changelog</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2016.02.20b
	<ul>
	<li>Pencarian barang keluar(alpha/not tested)</li>
	<li>Adding menu report convert to excel (XLS)</li>
	<li>Optimasi cache query laporan</li>
	<li>Update Version (Beta)</li>
	<li>Update Changelog</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2016.01.22b
	<ul>
	<li>Optimasi laporan dan rules(tester)</li>
	<li>Update Version (Beta)</li>
	<li>Update Changelog</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2016.01.13b
	<ul>
	<li>Fix bug laporan (tester)</li>
	<li>Update Version (Beta)</li>
	<li>Update Changelog</li>
	<li>Change Host Server</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2015.12.10b
	<ul>
	<li>Update Version (Beta)</li>
	<li>Revisi Decimal</li>
	<li>Revisi Formula</li>
	<li>Revisi Form Hasil</li>
	<li>Add Export Import Form Kontrak</li>
	<li>Fix Laporan Query</li>
	<li>Change URL Link CCTV</li>
	<li>Update Changelog</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2014.09.03b
	<ul>
	<li>Update Version Beta</li>
	<li>Revisi dan fix bug semua laporan beta test</li>
	<li>Revisi User Akses</li>
	<li>Revisi Form Kontrak,Proses,Hasil</li>
	<li>Revisi Jenis Barang</li>
	<li>Revisi Pengeluaran</li>
	<li>Update Menu CCTV</li>
	<li>Adding URL Link CCTV</li>
	<li>Adding Changelog dan Panduan</li>
	<li>Adding Rules</li>
	<li>Adding Currency</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2014.08.01b
	<ul>
	<li>Update Version Beta</li>
	<li>Update Laporan</li>
	<li>Update User Akses</li>
	<li>Update Laporan view to PDF</li>
	<li>Penambahan Data Gudang</li>
	<li>Penambahan Jenis Pekerjaan</li>
	<li>Penambahan User Divisi</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2014.07.06b
	<ul>
	<li>Update Version Beta</li>
	<li>Penambahan Master Data</li>
	<li>Fix Master Data Barang, Customer, Kelompok barang</li>
	<li>Fix Master Data Jenis barang,Satuan</li>
	<li>Fix dan Update Login Desain</li>
	<li>Penambahan User</li>
	</ul>
	</div>
	<div class="titleside">- SBC.SWAG.2014.06.05b
	<ul>
	<li>Perencanaan project dan desain</li>
	<li>Update Data Master</li>
	<li>Update Login</li>
	</ul>
	</div>
</div>
</body>
</html>
