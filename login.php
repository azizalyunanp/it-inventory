<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Sistem Informasi Export-Import</title>
<link rel="stylesheet" type="text/css" href="mycss/login.css" />
<script type="text/javascript" src="jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="libs_js/login.js"></script>
<script type="text/javascript" src="js/jquery-1.4.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".text").val('');
	$("#username").focus();
});
function validasi(form){
  if (form.username.value == ""){
    alert("Anda belum mengisikan Username.");
    form.username.focus();
    return (false);
  }
     
  if (form.password.value == ""){
    alert("Anda belum mengisikan Password.");
    form.password.focus();
    return (false);
  }
  return (true);
}
</script>
</head>
<title>.::Halaman Administrator::.</title>
<body>
<div class="lg-container">
		<h1>LOGIN SYBEA</h1>
		<form name="login" action="cek_login.php" method="POST" onSubmit="return validasi(this)">
			
		  <div>
			<label for="username">Username:</label>
			  <input type="text" name="username" id="username" placeholder="username"/>
		  </div>
			
			<div>
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" placeholder="password" />
			</div>
			
			<div>				
				<button type="submit" id="login">Login</button>
			</div>
			
		</form>
		<div id="message"></div>
</div>
</body>
</html>