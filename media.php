<?php
include 'inc/cek_session.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistem Persediaan Barang</title>
<link rel="stylesheet" href="css/icon.css" type="text/css" />
<link rel="stylesheet" href="css/superfish.css" type="text/css" />
<link rel="stylesheet" href="css/style_content.css" type="text/css" />
<link rel="stylesheet" href="css/style_tabel.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="mycss/index.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/panel.css" />

<script type="text/javascript" src="jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="jquery_easyui/jquery.easyui.min.js"></script>


<script>
function addTab(title, url){
			if ($('#tt').tabs('exists', title)){
				$('#tt').tabs('select', title);
			} else {
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
				$('#tt').tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}
		}
	</script>
	
	<script>
     var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 600000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 300000);
     }

     setTimeout(refresh, 300000);
</script>

	
	<script language="javascript">
var win = null;
function NewWindow(mypage,myname,w,h,scroll){
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
settings =
'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
win = window.open(mypage,myname,settings)
}
</script>

<script type="text/javascript" src="js/hoverIntent.js"></script>
<!-- untuk menu superfish -->
<script type="text/javascript" src="js/superfish.js"></script>

<!-- untuk datepicker -->
<link type="text/css" href="css/ui.all.css" rel="stylesheet" />   
<script type="text/javascript" src="js/ui.core.js"></script>
<script type="text/javascript" src="js/ui.datepicker.js"></script>
<script type="text/javascript" src="js/ui.datepicker-id.js"></script>

<!-- untuk autocomplite -->
<link rel="stylesheet" type="text/css" href="js/jquery.autocomplete.css" />
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>

<!-- plugin untuk tab -->
<link type="text/css" href="css/smoothness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	   $('ul.sf-menu').superfish();
  });
</script>
</head>
<body oncontextmenu='return false;' onkeydown='return false;' onmousedown='return false;'>
<div id="header">
<div class="panduan"><a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-help" plain="true" onclick="window.open('help.php','ChangeLog','width=900,height=600,scrollbars=yes,resizeable=no')"><b>BANTUAN DAN PANDUAN</b></a></div>
</div>
<div id="navigasi">
<div style="width:200px;height:auto;padding:5px;float:left; margin-right:10px;background:#7190E0;">
 <div id="aa" class="easyui-accordion" style="width:200px;height:auto;">
<div title="OLAH DATA MASTER" data-options="iconCls:'icon-ok'" style="overflow:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-report4" plain="true" onClick="addTab('Jenis Dokumen','jenis_dokumen.php')">Jenis Dokumen</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-supplier" plain="true" onClick="addTab('Data Customer','customer.php')">Data Customer</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box" plain="true" onClick="addTab('Jenis Pekerjaan','jenis_barang.php')">Jenis Pekerjaan</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box" plain="true" onClick="addTab('Kelompok Barang','kelompok_barang.php')">Kelompok Barang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Gudang','gudang.php')">Data Gudang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box-fill" plain="true" onClick="addTab('Data Barang','barang.php')">Data Barang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-sum" plain="true" onClick="addTab('Satuan','satuan.php')">Satuan</a><br />
<!-------<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Jenis Mutasi','jenis_mutasi.php')">Jenis Mutasi</a><br />------>
</div>

<div title="TRANSAKSI" data-options="iconCls:'icon-pdf'" style="overflow:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-cart" plain="true" onClick="addTab('Input Kontrak','order.php')">Input Kontrak</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-truck" plain="true" onClick="addTab('Input Barang Masuk','stpnb.php')">Input Barang Masuk</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Input Mutasi Proses','mutasi_proses.php')">Input Mutasi Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-stack-e" plain="true" onClick="addTab('Input Barang Dalam Proses','barang_proses.php')">Input Barang Dalam Proses</a><br/><br/>
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Input Mutasi Hasil','mutasi_hasil.php')">Input Mutasi Barang Jadi</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-truck" plain="true" onClick="addTab('Input Barang Keluar','bpgb.php')">Input Barang Keluar</a><br />
</div>

<div title="LAPORAN" data-options="iconCls:'icon-report2'" style="overflow:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Pemasukan','laporan/barang_masuk/pilih_lap.php')">Laporan Pemasukan</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Pengeluaran','laporan/barang_keluar/pilih_lap.php')">Laporan Pengeluaran</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan WIP','laporan/barang_proses/pilih_lap.php')">Laporan WIP</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Bahan Baku dan Penolong','laporan/mutasi_proses/pilih_lap.php')">Laporan Mutasi Bahan Baku dan Penolong</a><br /><br/>
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Barang Jadi','laporan/mutasi_hasil/pilih_lap.php')">Laporan Mutasi Barang Jadi</a><br /><br/>
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Mesin dan Peralatan','laporan/mutasi_proses/pilih_lap_mp.php')">Laporan Mutasi Mesin dan Peralatan</a><br /><br/>
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Barang Sisa dan Scrap','laporan/mutasi_proses/pilih_lap_sr.php')">Laporan Mutasi Barang Sisa dan Scrap</a><br /><br/>
</div>

<!--------<div title="CCTV" data-options="iconCls:'icon-search'" style="overflow:auto;padding:10px;">
<a href="http://swagcctv01.noip.me" class="easyui-linkbutton" target="_blank" iconCls="icon-report2" plain="true")>CCTV 1</a><br />
<a href="http://swagcctv02.noip.me" class="easyui-linkbutton" target="_blank" iconCls="icon-report2" plain="true")>CCTV 2</a><br />
</div>----------->

</div>

<div class="easyui-panel" data-options="iconCls:'icon-user'" title="AKSES" collapsible="true"  style="width:200px;height:auto;padding:10px;">
<a href="logout.php" class="easyui-linkbutton" iconCls="icon-logout" plain="true">Logout</a>
</div>
</div>
</div>
<div id="isi">
  <div id="tt" class="easyui-tabs" style="height:500px;">
<div title="Home" style="padding-top:20px; text-align:center; background-image:url(mycss/images/materials.png); background-repeat:no-repeat;background-color:#FFF;">
</div>
</div>
</div>
<div id="helper">
<strong><marquee>
Tampilan terbaik menggunakan Mozilla Firefox atau Google Chrome dengan resolusi 1366x768</marquee></strong>
</div>
<div id="footer">
SBC.SWAG.2016.02.20b | <a href="javascript:void(0);" class="easyui-linkbutton" plain="true" onclick="window.open('changelog.php','ChangeLog','width=600,height=400,scrollbars=yes,resizeable=no')">CHANGE LOG</a>
</div>
</body>
</html>