// JavaScript Document
$(document).ready(function() {
	$('#chk1').click(function() {
     $('#chk2').not('#chk1').removeAttr('checked');
	}); 
	
	$('#chk2').click(function() {
     $('#chk1').not('#chk2').removeAttr('checked');
	}); 
	
	//membuat text kode barang menjadi Kapital
	$("#txt_kodebrg").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kodebrg_op").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	
	$("#txt_tgl_pny").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_tgl_op").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});

	// format datepicker untuk tanggal
	$("#txt_tgl_pny").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	$("#txt_tgl_op").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	

	$("#txt_kodebrg").autocomplete("modul/penyesuaian/list_barang.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#txt_kodebrg_op").autocomplete("modul/penyesuaian/list_barang.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	// $("#txt_kodebrg").keyup(function() {
		// cari_kode();
	// });
	// $("#txt_kodebrg").focus(function() {
		// cari_kode();
	// });
	
	$("#txt_kodebrg").keyup(function() {
		cari_kode();
	});
	$("#txt_kodebrg").focus(function() {
		cari_kode();
	});
	
	$("#txt_kodebrg_op").keyup(function() {
		cari_kode2();
	});
	$("#txt_kodebrg_op").focus(function() {
		cari_kode2();
	});
	
	function cari_kode() {
		var kode	= $("#txt_kodebrg").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penyesuaian/cari_barang.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_nmbrg").val(data.nama_barang);
				// $("#txt_satuan").val(data.satuan);
				// $("#txt_kelompok").val(data.kelompok);
				// $("#txt_harga").val(data.harga);
			}
		});		
	}
	
	
	function cari_kode2() {
		var kode	= $("#txt_kodebrg_op").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penyesuaian/cari_barang.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_nmbrg_op").val(data.nama_barang);
			}
		});		
	}
	
	 /* angka desimal yang dapat dientry */
$		('#txt_pny').keypress(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	/* hanya angka yang dapat dientry */
	$("#txt_pny").keypress(function(data){
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	
	function kosong(){
		$(".detail_readonly").val('');
		$(".input_detail").val('');
	}
	
	$("#simpan").unbind().click(function(){
		var kode_barang=$("#txt_kodebrg").val();	
		var tgl=$("#txt_tgl_pny").val();	
		var nama_barang=$("#txt_nmbrg").val();
			if($('#chk1').attr('checked')) {
				var check= $("#chk1").val();
				} else {
				var check= $("#chk2").val();
			}
		var jumlah= $("#txt_pny").val();
		
		// var error = false;
		// if(kode_barang.length==0){
           // var error = true;
           // alert("Maaf, Kode barang tidak boleh kosong");
		   // $("#txt_kodebrg").focus();
		   // return (false);
         // }
		var error = false;
		 if(kode_barang==''){
			var error = true;
			alert("Maaf, Kode barang tidak boleh kosong");
			$("#txt_kodebrg").focus();
			return (false);
         }
		  checked = $("input[type=checkbox]:checked").length;
		  if(!checked) {
			alert("Pilih salah satu checkbox");
			return false;
		  }
		 if(tgl==''){
			var error = true;
			alert("Maaf, Tanggal harus di pilih");
			$("#txt_tgl_pny").focus();
			return (false);
        }
		 if(jumlah==''){
			var error = true;
			alert("Maaf, Jumlah harus di isi");
			$("#txt_pny").focus();
			return (false);
        }
		 		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/penyesuaian/simpan.php",
			data	: "kode_barang="+kode_barang+
						"&tgl="+tgl+
						"&nama_barang="+nama_barang+
						"&check="+check+
						"&jumlah="+jumlah,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info").show();
				$("#info").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info").show();
				$("#info").html(data);
				$('#info').delay(1000).fadeOut();
			}
		});
		}
		return false; 
	});
	
	
	$("#simpan_opname").unbind().click(function(){
		var kode_barang_op=$("#txt_kodebrg_op").val();	
		var tgl_op=$("#txt_tgl_op").val();	
		var gudang=$("#txt_gudang").val();
		var nama_barang_op=$("#txt_nmbrg_op").val();
		var jumlah_op= $("#txt_op").val();
		
		
		var error = false;
		 if(kode_barang_op==''){
			var error = true;
			alert("Maaf, kode barang tidak boleh kosong");
			$("#txt_kodebrg_op").focus();
			return (false);
        }
		
		if(tgl_op==''){
			var error = true;
			alert("Maaf, tanggal tidak boleh kosong");
			$("#txt_tgl_op").focus();
			return (false);
        }
		
		if(gudang==''){
			var error = true;
			alert("Maaf, gudang harus di pilih");
			$("#txt_gudang").focus();
			return (false);
        }
		
		if(jumlah_op==''){
			var error = true;
			alert("Maaf, jumlah harus di isi");
			$("#txt_op").focus();
			return (false);
        }
		 
		 		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/penyesuaian/simpan_opname.php",
			data	: "kode_barang_op="+kode_barang_op+
						"&tgl_op="+tgl_op+
						"&nama_barang_op="+nama_barang_op+
						"&gudang="+gudang+
						"&jumlah_op="+jumlah_op,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#infr").show();
				$("#infr").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#infr").show();
				$("#infr").html(data);
				$('#infr').delay(1000).fadeOut();
			}
		});
		}
		return false; 
	});
	
		$("#hapus_bukti").unbind().click(function() {
		var pilih = confirm('Data ini akan dihapus ?');
		if (pilih==true) {
		var kode_barang=$("#txt_kodebrg").val();	
		var tgl=$("#txt_tgl_pny").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penyesuaian/hapus.php",
			data	: "kode_barang="+kode_barang+
						"&tgl="+tgl,
			success	: function(data){
				$("#info").show();
				$("#info").html(data);
				$("#txt_tgl_pny").val('');
				$("#txt_kodebrg").val('');
				$("#txt_nmbrg").val('');
				$('#chk1').attr("checked", false);
				$('#chk2').attr("checked", false);
				$("#txt_pny").val('');
				$('#info').delay(1000).fadeOut();				

			}
		});
		}
	});
	
	
		$("#hapus_bukti_opname").unbind().click(function() {
		var pilih = confirm('Data ini akan dihapus?');
		if (pilih==true) {
			var kode_barang_op=$("#txt_kodebrg_op").val();	
			var tgl_op=$("#txt_tgl_op").val();
			$.ajax({
				type	: "POST",
				url		: "modul/penyesuaian/hapus_opname.php",
				data	: "kode_barang_op="+kode_barang_op+
							"&tgl_op="+tgl_op,
				success	: function(data){
					$("#infr").show();
					$("#infr").html(data);
					$("#txt_tgl_op").val('');
					$("#txt_kodebrg_op").val('');
					$("#txt_nmbrg_op").val('');
					$("#txt_gudang").val('');
					$("#txt_op").val('');
					$('#infr').delay(1000).fadeOut();
					}
				});
			}
		});



		
});

