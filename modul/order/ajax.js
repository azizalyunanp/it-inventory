// JavaScript Document
$(document).ready(function() {
	//membuat text kode barang menjadi Kapital
	$("#kode_barang").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kode_kontrak").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kode_beli").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});

	// format datepicker untuk tanggal
	$("#txt_tgl_beli").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
		// format datepicker untuk tanggal
	$("#txt_tgl_jt").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
/* 	//hanya angka yang dapat dientry
	$("#txt_jumlah").keypress(function(data){
		if (data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) 
		{
			return false;
		}
	});
 */

 	 /* angka desimal yang dapat dientry */
$		('#txt_jumlah').keypress(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	/* hanya angka yang dapat dientry */
	$("#txt_total").keypress(function(data){
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	
	function kosong(){
		$(".detail_readonly").val('');
		$(".input_detail").val('');
	}
		
	
	function cari_nomor() {
		var no		=1;
		$.ajax({
			type	: "POST",
			url		: "modul/order/cari_nomor.php",
			data	: "no="+no,
			dataType : "json",
			success	: function(data){
				$("#txt_kode_beli").val(data.nomor);
				tampil_data();
			}
		});		
	}

	function tampil_data() {
		var kode 	= $("#txt_kode_beli").val();;
		$.ajax({
				type	: "POST",
				url		: "modul/order/tampil_data.php",
				data	: "kode="+kode,
				timeout	: 3000,
				beforeSend	: function(){		
					$("#info").html("<img src='loading.gif' />");			
				},				  
				success	: function(data){
					$("#info").html(data);
				}
		});			
	}

	
	cari_nomor();
	
	$("#txt_kode_barang").autocomplete("modul/order/list_barang.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#txt_kode_beli").autocomplete("modul/order/list_bukti.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	

	function cari_bukti() {
		var kode	= $("#txt_kode_beli").val();
		$.ajax({
			type	: "POST",
			url		: "modul/order/cari_kode.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_dok").val(data.tipe);
				$("#txt_tgl_beli").val(data.tgl);
				$("#txt_style").val(data.style);
				$("#txt_jenis").val(data.pusbi);
				$("#cbo_supplier").val(data.id_supplier);
				$("#txt_kode_kontrak").val(data.no_kontrak);
				$("#txt_ketr").val(data.keterangan);
				$("#txt_tgl_jt").val(data.jatuh_tempo);
				tampil_data();
			}
		});		
	}
	
	
	function cari_kode() {
		var kode	= $("#txt_kode_barang").val();
		$.ajax({
			type	: "POST",
			url		: "modul/order/cari_barang.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_nama_barang").val(data.nama_barang);
				$("#txt_kelompok").val(data.kelompok);
				$("#txt_satuan").val(data.satuan);
				$("#txt_harga").val(data.harga);
			}
		});		
	}
	
	
	$("#txt_kode_barang").keyup(function() {
		cari_kode();
	});
	$("#txt_kode_barang").focus(function() {
		cari_kode();
	});
	
	$("#txt_kode_beli").keyup(function() {
		cari_bukti();
	});
	$("#txt_kode_beli").focus(function() {
		cari_bukti();
	});
	
	//mengalikan jumlah dengan harga
	$("#txt_jumlah").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var harga	= $("#txt_harga").val();
		if (jml.length!='') {
			if ($("#txt_dok").val()=='11'){
				var total	= parseInt(jml)*harga;
/* 				var disc	= total*0.02;
				var total2	= total-disc;
				$("#txt_total").val(total2); */
				$("#txt_total").val(total);
			}else{			
				var total	= parseInt(jml)*harga;
				var disc	= total*0.02;
				var total2	= total-disc;
				$("#txt_total").val(total2);
			/* 	$("#txt_total").val(total); */
			}
		}else{
			$("#txt_total").val(0);
		}
	});
	
	//membagi harga total
	$("#txt_total").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var total	= $("#txt_total").val();
		if (jml.length!='') {
			var harga	= total/parseInt(jml);
			$("#txt_harga").val(harga);
		}
	});


	$("#tambah_detail").click(function(){
		kosong();	
		$("#txt_kode_barang").focus();
	});


	$("#tambah_detail").click(function(){
		kosong();	
		$("#txt_kode_barang").focus();
	});

	
	$("#simpan").click(function(){
		var kode			= $("#txt_kode_beli").val();	
		var tgl				= $("#txt_tgl_beli").val();	
		var supplier		= $("#cbo_supplier").val();	
		var	kode_barang		= $("#txt_kode_barang").val();
		var	nama_barang		= $("#txt_nama_barang").val();
		var	satuan			= $("#txt_satuan").val();
		var	jumlah			= $("#txt_jumlah").val();
		var harga			= $("#txt_harga").val();	
		var kelompok		= $("#txt_kelompok").val();
		var style			= $("#txt_style").val();
		var pusbi			= $("#txt_jenis").val();
		var nilai			= $("#txt_total").val();
		var no_kontrak		= $("#txt_kode_kontrak").val();
		var keterangan		= $("#txt_ketr").val();
		var tgl_jt			= $("#txt_tgl_jt").val();
		var cur 			= $("#txt_cur").val();
		var tipe			= $("#txt_dok").val();
		
		
		var error = false;
		if(kode.length == 0){
           var error = true;
           alert("Maaf, Kode Pembelan tidak boleh kosong");
		   $("#txt_kode_beli").focus();
		   return (false);
         }
		if(tgl.length == 0){
           var error = true;
           alert("Maaf, Tanggal order tidak boleh kosong");
		   $("#txt_tgl_beli").focus();
		   return (false);
         }
		 
		 	 if(no_kontrak.length == 0){
           var error = true;
           alert("Maaf, Nomor Kontrak tidak boleh kosong");
		   $("#txt_kode_kontrak").focus();
		   return (false);
         }
		 
		 if(pusbi.length == 0){
           var error = true;
           alert("Maaf, Jenis Pekerjaan tidak boleh kosong");
		   $("#txt_jenis").focus();
		   return (false);
         }
		 
		if(style.length == 0){
           var error = true;
           alert("Maaf, Style tidak boleh kosong");
		   $("#txt_style").focus();
		   return (false);
         }
		 
		if(supplier.length == 0){
           var error = true;
           alert("Maaf, Supplier tidak boleh kosong");
		   $("#cbo_supplier").focus();
		   return (false);
         }
		 
		if(kode_barang.length == 0){
           var error = true;
           alert("Maaf, Kode barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(nama_barang.length == 0){
           var error = true;
           alert("Maaf, Nama Barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(jumlah.length == 0){
           var error = true;
           alert("Maaf, Jumlah Barang tidak boleh kosong");
		   $("#txt_jumlah").focus();
		   return (false);
         }
	

		 		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/order/simpan.php",
			data	: "kode="+kode+
						"&tgl="+tgl+
						"&supplier="+supplier+
						"&kode_barang="+kode_barang+
						"&nama_barang="+nama_barang+
						"&no_kontrak="+no_kontrak+
						"&kelompok="+kelompok+
						"&jumlah="+jumlah+
						"&harga="+harga+
						"&style="+style+
						"&satuan="+satuan+
						"&tipe="+tipe+
						"&nilai="+nilai+
						"&cur="+cur+
						"&pusbi="+pusbi+
						"&tgl_jt="+tgl_jt+
						"&keterangan="+keterangan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info").show();
				$("#info").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info").show();
				$("#info").html(data);
			}
		});
		}
		return false; 
	});

	
	$("#tambah_beli").click(function() {
		$(".input").val('');
		$(".input").removeAttr("readonly");
		$('#txt_kode_beli').attr('readonly','true');
		kosong();
		cari_nomor();
		$("#txt_tgl_beli").focus();
	});
	
	$("#hapus_bukti").click(function() {
		var pilih = confirm('Bukti ini akan dihapus ?');
		if (pilih==true) {
		var kode	= $("#txt_kode_beli").val();
		$.ajax({
			type	: "POST",
			url		: "modul/order/hapus_bukti.php",
			data	: "kode="+kode,
			success	: function(data){
				$("#info").html(data);
				tambah_beli();
			}
		});
		}
	});

		
	$("#edit").click(function() {
		$("#txt_kode_beli").removeAttr("readonly");
		$('.input').attr('readonly','true');
		$('#txt_kode_beli').val('');
		$("#txt_kode_beli").focus();
	});

	$("#keluar").click(function(){
		window.close();
	});

});

