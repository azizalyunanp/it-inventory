// JavaScript Document
$(document).ready(function() {
	//membuat text kode barang menjadi Kapital
	$("#kode_barang").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_no_kontrak").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});

	// format datepicker untuk tanggal
	$("#txt_tgl_beli").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	$("#txt_tgl_invoice").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
		
	$("#txt_tgl_pabean").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
			
	$("#txt_tgl_skep").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});

	$("#txt_tgl_packing").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
			
	$("#txt_tgl_suratjalan").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	//hanya angka yang dapat dientry
	$("#txt_jumlah").keypress(function(data){
		if (data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) 
		{
			return false;
		}
	});


	function kosong(){
		$(".detail_readonly").val('');
		$(".input_detail").val('');
	}
	
	function cari_nomor() {
		var no		=1;
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/cari_nomor.php",
			data	: "no="+no,
			dataType : "json",
			success	: function(data){
				$("#txt_kode_beli").val(data.nomor);
				tampil_data();
			}
		});		
	}

	function tampil_data() {
		var kode 	= $("#txt_kode_beli").val();;
		$.ajax({
				type	: "POST",
				url		: "modul/stpnb/tampil_data.php",
				data	: "kode="+kode,
				timeout	: 3000,
				beforeSend	: function(){		
					$("#info").html("<img src='loading.gif' />");			
				},				  
				success	: function(data){
					$("#info").html(data);
				}
		});			
	}

	
	cari_nomor();
	
	$("#txt_kode_barang").autocomplete("modul/stpnb/list_barang.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#txt_no_kontrak").autocomplete("modul/stpnb/list_kontrak.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	function cari_kode() {
		var kode	= $("#txt_kode_barang").val();
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/cari_barang.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_nama_barang").val(data.nama_barang);
				$("#txt_satuan").val(data.satuan);
				$("#txt_kelompok").val(data.kelompok);
				$("#txt_harga").val(data.harga);
			}
		});		
	}
	
	$("#txt_kode_barang").keyup(function() {
		cari_kode();
	});
	$("#txt_kode_barang").focus(function() {
		cari_kode();
	});
	
	$("#txt_no_kontrak").keyup(function() {
		cari_kode();
	});
	$("#txt_no_kontrak").focus(function() {
		cari_kode();
	});
	
	//mengalikan jumlah dengan harga
	$("#txt_jumlah").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var harga	= $("#txt_harga").val();
		if (jml.length!='') {
			var total	= parseInt(jml)*parseInt(harga);
			$("#txt_total").val(total);
		}else{
			$("#txt_total").val(0);
		}
	});


	$("#tambah_detail").click(function(){
		kosong();	
		$("#txt_kode_barang").focus();
	});

	
	$("#simpan").click(function(){
		var kode			= $("#txt_kode_beli").val();	
		var tgl				= $("#txt_tgl_beli").val();	
		var supplier		= $("#cbo_supplier").val();	
		var	kode_barang		= $("#txt_kode_barang").val();
		var	nama_barang		= $("#txt_nama_barang").val();
		var	satuan			= $("#txt_satuan").val();
		var	jumlah			= $("#txt_jumlah").val();
		var harga			= $("#txt_harga").val();
		var kelompok		= $("#txt_kelompok").val();
		var total			= $("#txt_total").val();
		var jenisdok		= $("#txt_jenis_dokumen").val();
		var	tgl_invoice		= $("#txt_tgl_invoice").val();
		var	tgl_pabean		= $("#txt_tgl_pabean").val();
		var	tgl_skep		= $("#txt_tgl_skep").val();
		var	tgl_packing		= $("#txt_tgl_packing").val();
		var	tgl_suratjalan	= $("#txt_tgl_suratjalan").val();
		var	invoice			= $("#txt_no_invoice").val();
		var	pabean			= $("#txt_no_pabean").val();
		var	skep			= $("#txt_no_skep").val();
		var	packing			= $("#txt_no_packing").val();
		var	suratjalan		= $("#txt_no_suratjalan").val();
		var kontrak 		= $("#txt_no_kontrak").val();
		var keterangan		= $("#txt_keterangan").val();
		
		var error = false;
		if(kode.length == 0){
           var error = true;
           alert("Maaf, Kode Pembelan tidak boleh kosong");
		   $("#txt_kode_beli").focus();
		   return (false);
         }
		if(tgl.length == 0){
           var error = true;
           alert("Maaf, Tanggal stpnb tidak boleh kosong");
		   $("#txt_tgl_beli").focus();
		   return (false);
         }
		if(supplier.length == 0){
           var error = true;
           alert("Maaf, Supplier tidak boleh kosong");
		   $("#cbo_supplier").focus();
		   return (false);
         }
		if(kode_barang.length == 0){
           var error = true;
           alert("Maaf, Kode barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(nama_barang.length == 0){
           var error = true;
           alert("Maaf, Nama Barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(jumlah.length == 0){
           var error = true;
           alert("Maaf, Jumlah Barang tidak boleh kosong");
		   $("#txt_jumlah").focus();
		   return (false);
         }

		 		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/simpan.php",
			data	: "kode="+kode+
						"&tgl="+tgl+
						"&supplier="+supplier+
						"&kode_barang="+kode_barang+
						"&nama_barang="+nama_barang+
						"&kelompok="+kelompok+
						"&satuan="+satuan+
						"&jumlah="+jumlah+
						"&harga="+harga+
						"&total="+total+
						"&jenisdok="+jenisdok+
						"&invoice="+invoice+
						"&tgl_invoice="+tgl_invoice+
						"&pabean="+pabean+
						"&tgl_pabean="+tgl_pabean+
						"&skep="+skep+
						"&tgl_skep="+tgl_skep+
						"&packing="+packing+
						"&tgl_packing="+tgl_packing+
						"&suratjalan="+suratjalan+
						"&tgl_suratjalan="+tgl_suratjalan+
						"&kontrak="+kontrak+
						"&keterangan="+keterangan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info").show();
				$("#info").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info").show();
				$("#info").html(data);
			}
		});
		}
		return false; 
	});

	$("#tambah_beli").click(function() {
		$(".input").val('');
		kosong();
		cari_nomor();
		$("#txt_tgl_beli").focus();
	});

	$("#keluar").click(function(){
		window.close();
	});

});

