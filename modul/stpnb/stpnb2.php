<script type="text/javascript">	
$(document).ready(function() {
	//$("#tabs").tabs();
	$("#tabs").tabs({selected:0});
	//$("#tabs").tabs({collapsible:true});
	$("#form").load('modul/stpnb/form.php');
	$("#tampil_data").load('modul/stpnb/tampil_data.php');
	$("#info_jual").hide();
	
	$("#txt_no_kontrak").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_no_kontrak").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_no_kontrak").autocomplete("modul/stpnb/list_kontrak.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#cari_tgl").click(function() {
		var tgl_awal = $("#cari_tgl_awal").val();
		var tgl_akhir = $("#cari_tgl_akhir").val();
		
		if(tgl_awal.length==''){
			alert('Maaf, Variabel tanggal belum lengkap');
			$("#cari_tgl_awal").focus();
			return false;
		}
		if(tgl_akhir.length==''){
			alert('Maaf, Variabel tanggal belum lengkap');
			$("#cari_tgl_akhir").focus();
			return false;
		}
		$("#info_jual").hide();
		$("#pencarian").show();
		$("#tampil_data").load('modul/stpnb/tampil_data2.php?tgl_awal='+tgl_awal+'&tgl_akhir='+tgl_akhir);
	});
	
	$("#cari_kontrak").click(function() {
		var no_kontrak = $("#txt_no_kontrak").val();
		
		$("#info_jual").hide();
		$("#pencarian").hide();
		$("#kontrakk").show();
		$("#cari_kontrak").hide();
		$("#tampil_data").load('modul/stpnb/tampil_data_kontrak.php?no_kontrak='+no_kontrak);
	});
	
	$("#tutup_detail").click(function() {
		$("#pencarian").show();
		$("#info_jual").hide();
		$("#tampil_data").load('modul/stpnb/tampil_data2.php?');
	});
	
		$("#cetak").click(function() {
		$("#cetak").hide();

	});
	
		// format datepicker untuk tanggal
	$("#cari_tgl_awal").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	$("#cari_tgl_akhir").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	
});
</script>

<style type="text/css">
#info {
	font-size:12px;
	font-weight:bold;
	color:#F00;
}
</style>

<style type="text/css">
      input { border: 1px solid }
      /* other style definitions go here */
	  

    </style>
<?php
echo "<div id='tabs'>";
echo "";
	echo "<div id='data'>
			<div id='pencarian'>
			<table width='100%'>
				<tr>
					<td width='20%'>Filter Tanggal</td>
					<td width='2%'>:</td>
					<td>Dari &nbsp; <input type='text' name='cari_tgl_awal' id='cari_tgl_awal' size='12'> &nbsp; sampai &nbsp;
					<input type='text' name='cari_tgl_akhir' id='cari_tgl_akhir' size='12'> &nbsp; &nbsp;
					<button name='cari_tgl' id='cari_tgl'>CARI</button>
					</td>
				</tr>
			</table>
			</div>
			<div id='kontrakk'>
			<table width='100%'>
				<tr>
					<td width='20%'>Filter Berdasar No Kontrak</td>
					<td width='2%'>:</td>
					<td>Masukkan No Kontrak &nbsp; : &nbsp; <input type='text' name='txt_no_kontrak' id='txt_no_kontrak' size='24'>
					<button name='cari_kontrak' id='cari_kontrak'>CARI</button>
					</td>
				</tr>
			</table>
			</div>
			<div id='info_jual'>
			<img src='mycss/images/logo.png' alt=''>
			<table width='100%'>
				<tr>
					<td width='20%'>Kode</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_kode_jual' id='lbl_kode_jual' size='20' readonly></td>
					<td width='20%'>Tanggal</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_tgl_jual' id='lbl_tgl_jual' size='10' readonly></td>						
				</tr>
				<tr>
				
					<td width='20%'>Customer</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_customer' id='lbl_customer' size='20' readonly></td>	
					<td width='20%'>Jenis Dokumen</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_jenis_dokumen' id='lbl_jenis_dokumen' size='10' readonly></td>	

				</tr>
				<tr>
				
					<td width='20%'>No Kontrak</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_no_kontrak' id='lbl_no_kontrak' size='30' readonly></td>	
				</tr>
				<tr>
					<td width='20%'>No Invoice</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_no_invoice' id='lbl_no_invoice' size='20' readonly></td>	
					<td width='20%'>Tgl Invoice</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_tgl_invoice' id='lbl_tgl_invoice' size='10' readonly></td>	
				</tr>
				<tr>
					<td width='20%'>No Pabean</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_no_pabean' id='lbl_no_pabean' size='20' readonly></td>	
					<td width='20%'>Tgl Pabean</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_tgl_pabean' id='lbl_tgl_pabean' size='10' readonly></td>	
				</tr>
								<tr>
					<td width='20%'>No Packing</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_no_packing' id='lbl_no_packing' size='20' readonly></td>	
					<td width='20%'>Tgl Packing</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_tgl_packing' id='lbl_tgl_packing' size='10' readonly></td>	
				</tr>
								<tr>
					<td width='20%'>No Skep</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_no_skep' id='lbl_no_skep' size='20' readonly></td>	
					<td width='20%'>Tgl Skep</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_tgl_skep' id='lbl_tgl_skep' size='10' readonly></td>	
				</tr>
								<tr>
					<td width='20%'>No Surat Jalan</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_no_suratjalan' id='lbl_no_suratjalan' size='20' readonly></td>	
					<td width='20%'>Tgl Surat Jalan</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_tgl_suratjalan' id='lbl_tgl_suratjalan' size='10' readonly></td>	
				</tr>
				<tr>	
					<td width='20%'>Keterangan</td>
					<td width='2%'>:</td>
					<td><input type='text' name='lbl_keterangan' id='lbl_keterangan' size='40' readonly></td>			
				</tr>
					</td>					
				</tr>
			</table>			
			</div>
			<br/>
			<HR WIDTH=100%>
			<div id='tampil_data' align=''></div>
			<br/>
			<br/>
			<div id='footer' align='right'>
					<tr>
					<td>EXIM&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp                                    
					</td>
					<td>Manager&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
					</td>
					</tr>					
			</div>
			<br/><br/><br/>
			<div id='footer' align='right'>
					<tr>
						<td>_____________&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
						<td>_____________&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
					</tr>	
			</div>
			<br/>
			<br/>
			<div id='back' align='left'>
				<a href='javascript:history.go(0)'><img src='icon/back.gif' border='0' id='back' title='back' width='30' height='30'></a>
				<a href='javascript:void(0)' onClick='window.print();'>
					<img src='icon/download.jpg' border='0' id='print' title='print' width='30' height='30' >
			</div>
	</div>";
	echo "<div id='info' align='center'></div>";
echo "</div>";
?>
