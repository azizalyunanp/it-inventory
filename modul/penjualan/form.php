<?
session_start();
?>
<script type="text/javascript">
$(document).ready(function() {
	$("#txt_kode_barang").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kode").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kontrak").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kontrak").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
						   
	$("#txt_tgl_jual").datepicker({
		dateFormat      : "dd-mm-yy",        
	  	showOn          : "button",
	  	buttonImage     : "images/calendar.gif",
	  	buttonImageOnly : true				
	});
	
	$("#txt_tgl_pabean").datepicker({
		dateFormat      : "dd-mm-yy",        
	  	showOn          : "button",
	  	buttonImage     : "images/calendar.gif",
	  	buttonImageOnly : true				
	});
	
/* 	$("#txt_jumlah").keypress(function(data){
		if (data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) 
		{
			return false;
		}
	}); */
	
		 /* angka desimal yang dapat dientry */
$		('#txt_jumlah').keypress(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	/* hanya angka yang dapat dientry */
	$("#txt_total").keypress(function(data){
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	

	$('#form_cari_barang').dialog({
		autoOpen: false,
		modal	: true,
		width: 600,
	});
	
	// Dialog Link
	$('#list_barang').click(function(){
		$('#form_cari_barang').dialog('open');
		return false;
	});
	cari_nomor();
	
	function cari_nomor(){
		var no = 1;
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/cari_nomor.php",
			data	: "no="+no,
			dataType: "json",
			success	: function(data){
				$("#txt_kode").val(data.nomor);
				tampil_data_detail();
			}
		});
	}
	
	function tampil_data_detail(){
		var kode = $("#txt_kode").val();
		$("#data_detail").load("modul/penjualan/tampil_data_detail.php?kode="+kode);
	}
	
	$("#tambah_barang").click(function(){
		$(".input_detail").val('');
		$("#txt_kode_barang").focus();
	});
	
	$("#txt_kode_barang").keyup(function() {
		var kode 	= $("#txt_kode_barang").val()
		//var akses	= 1; //1 cari, 2 edit, 3 baru
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/cari_barang.php",
			data	: "kode="+kode,
			dataType	: "json",
			success	: function(data){
				$("#txt_nama_barang").val(data.nama_barang);
				$("#txt_kelompok").val(data.kelompok);
				$("#txt_satuan").val(data.satuan);
				$("#txt_harga").val(data.harga);
			}
		});
	});

	function cari_bukti() {
		var kode	= $("#txt_kode").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/cari_kode.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_dok").val(data.tipe);
				$("#txt_tgl_jual").val(data.tgl);
				$("#txt_kontrak").val(data.no_kontrak);
				$("#txt_tgl_kontrak").val(data.tgl_kontrak);
				$("#txt_pabean").val(data.no_pabean);
				$("#txt_tgl_pabean").val(data.tgl_pabean);
				$("#txt_customer").val(data.id_supplier);
				$("#txt_gudang").val(data.gudang);
				$("#txt_jenis_dokumen").val(data.id_jenis);
				$("#txt_keterangan").val(data.keterangan);
				tampil_data_detail();
			}
		});		
	}
	
	$("#txt_kode").keyup(function() {
		cari_bukti();
	});
	$("#txt_kode").focus(function() {
		cari_bukti();
	});	
	
	$("#txt_kode").autocomplete("modul/penjualan/list_bukti.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	function cari_kontrak() {
		var kode	= $("#txt_kontrak").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/cari_kontrak.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_tgl_kontrak").val(data.tgl);
				$("#txt_kontrak").val(data.no_kontrak);
				tampil_data_detail();
			}
		});		
	}
	
	$("#txt_kontrak").keyup(function() {
		cari_kontrak();
	});
	$("#txt_kontrak").focus(function() {
		cari_kontrak();
	});
	
	
	$("#txt_kontrak").autocomplete("modul/penjualan/list_kontrak.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	
	/* function cari_pabean() {
		var kode	= $("#txt_pabean").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/cari_pabean.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_tgl_pabean").val(data.tgl);
				$("#txt_pabean").val(data.no_pabean);
				tampil_data_detail();
			}
		});		
	}
	
	$("#txt_pabean").keyup(function() {
		cari_pabean();
	});
	$("#txt_pabean").focus(function() {
		cari_pabean();
	});
	
	
	$("#txt_pabean").autocomplete("modul/penjualan/list_pabean.php", {
				width:100,
				max: 10,
				scroll:false,
	}); */
	
	$("#txt_jumlah").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var harga	= $("#txt_harga").val();
		if (jml.length!='') {
			if ($("#txt_dok").val()=='11'){
				var total	= parseInt(jml)*harga;
/* 				var disc	= total*0.02;
				var total2	= total-disc;
				$("#txt_total").val(total2); */
				$("#txt_total").val(total);
			}else{			
				var total	= parseInt(jml)*harga;
				var disc	= total*0.02;
				var total2	= total-disc;
				$("#txt_total").val(total2);
			/* 	$("#txt_total").val(total); */
			}
		}else{
			$("#txt_total").val(0);
		}
	});
	
	
	//membagi harga total
	$("#txt_total").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var total	= $("#txt_total").val();
		if (jml.length!='') {
			var harga	= total/parseInt(jml);
			$("#txt_harga").val(harga);
		}
	});


	$("#tambah_detail").click(function(){
		kosong();	
		$("#txt_kode_barang").focus();
	});

	$("#simpan").click(function(){
		simpan_data();
	});

	function simpan_data(){
		var	kode		= $("#txt_kode").val();
		var	tgl			= $("#txt_tgl_jual").val();
		var	tgl_kontrak	= $("#txt_tgl_kontrak").val();
		var	no_pabean	= $("#txt_pabean").val();
		var	tgl_pabean	= $("#txt_tgl_pabean").val();
		var	kode_brg	= $("#txt_kode_barang").val();
		var	nama_brg	= $("#txt_nama_barang").val();
		var	jumlah		= $("#txt_jumlah").val();
		var	harga		= $("#txt_harga").val();
		var satuan		= $("#txt_satuan").val();
		var total		= $("#txt_total").val();
		var no_kontrak	= $("#txt_kontrak").val();
		var id_customer	= $("#txt_customer").val();
		var id_jenis	= $("#txt_jenis_dokumen").val();
		var kelompok	= $("#txt_kelompok").val();
		var keterangan	= $("#txt_keterangan").val();
		var gudang		= $("#txt_gudang").val();
		var cur		= $("#txt_cur").val();
		var tipe	= $("#txt_dok").val();
		
		var error = false;

		if(kode.length == 0){
           var error = true;
           alert("Maaf, Kode Penjualan tidak boleh kosong");
		   $("#txt_kode").focus();
		   return (false);
         }
		if(tgl.length == 0){
           var error = true;
           alert("Maaf, Tanggal tidak boleh kosong");
		   //$("#txt_nama").focus();
		   return (false);
         }	 
		 if(no_kontrak.length == 0){
           var error = true;
           alert("Maaf, No Kontrak tidak boleh kosong");
		   $("#txt_kontrak").focus();
		   return (false);
         }
		 
		 if(id_customer.length == 0){
           var error = true;
           alert("Maaf, Customer tidak boleh kosong");
		   $("#txt_customer").focus();
		   return (false);
         }
		 
		 if(gudang.length == 0){
           var error = true;
           alert("Maaf, Gudang tidak boleh kosong");
		   $("#txt_gudang").focus();
		   return (false);
         }
		 
		if(tgl_pabean.length == 0){
           var error = true;
           alert("Maaf, Tanggal pabean tidak boleh kosong");
		   $("#txt_gudang").focus();
		   return (false);
         }
		 
		if(no_pabean.length == 0){
           var error = true;
           alert("Maaf, Nomor Pabean tidak boleh kosong");
		   $("#txt_gudang").focus();
		   return (false);
         }
		 
		if(kode_brg.length == 0){
           var error = true;
           alert("Maaf, Kode Barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(nama_brg.length == 0){
           var error = true;
           alert("Maaf, Nama Barang tidak boleh kosong");
		   $("#txt_nama_barang").focus();
		   return (false);
         }
		if(jumlah.length == 0){
           var error = true;
           alert("Maaf, Jumlah Barang tidak boleh kosong");
		   $("#txt_jumlah").focus();
		   return (false);
         }
		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/simpan.php",
			data	: "kode="+kode+
					"&tgl="+tgl+
					"&kode_brg="+kode_brg+
					"&nama_brg="+nama_brg+
					"&satuan="+satuan+
					"&cur="+cur+
					"&tipe="+tipe+
					"&jumlah="+jumlah+
					"&total="+total+
					"&kelompok="+kelompok+
					"&no_kontrak="+no_kontrak+
					"&tgl_kontrak="+tgl_kontrak+
					"&no_pabean="+no_pabean+
					"&tgl_pabean="+tgl_pabean+
					"&id_customer="+id_customer+
					"&id_jenis="+id_jenis+
					"&gudang="+gudang+
					"&harga="+harga+
					"&keterangan="+keterangan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info").html(data);
				tampil_data_detail();
			}
		});
		}
		return false; 	
	
	}
	
	$("#tambah").click(function(){
		cari_nomor();
		$(".input_detail").val('');
		$(".input").val('');
		$(".input").removeAttr("readonly");
		$('#txt_kode').attr('readonly','true');
		kosong();
		cari_nomor();
		$("#txt_kontrak").focus();
		$("#txt_tgl_jual").val('');
	});

	$("#hapus_bukti").click(function() {
		var pilih = confirm('Bukti ini akan dihapus ?');
		if (pilih==true) {
		var kode	= $("#txt_kode").val();
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/hapus_bukti.php",
			data	: "kode="+kode,
			success	: function(data){
				$("#info").html(data);
				$(".detail_readonly").val('');
				$(".input_detail").val('');
			}
		});
		}
	});

		
	$("#edit").click(function() {
		$("#txt_kode").removeAttr("readonly");
		$('.input').attr('readonly','true');
		$('#txt_kode').val('');
		$("#txt_kode").focus();
	});

	$("#keluar").click(function(){
		window.close();
	});
	
	
	$("#txt_cari").keyup(function(){
		var cari	= $("#txt_cari").val();								  
		$.ajax({
			type	: "POST",
			url		: "modul/penjualan/tampil_data_barang.php",
			data	: "cari="+cari,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info_barang").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info_barang").html(data);
			}
		});
	});

	
	
});
</script>
<style type="text/css">
#txt_kode {
	background:#CCC;
}
#list_barang{
	cursor:pointer;
}
.input_detail readonly {
	background:#666;
}
</style>
<?php
include '../../inc/inc.koneksi.php';


$row = $_SESSION['namauser'];
$lv	= $_SESSION['leveluser'];


echo "<table width='100%'>
	<tr>
		<td width='15%'>Tipe</td>
		<td width='2%'>:</td>
			<td><select name='txt_dok' id='txt_dok' class='input' onchange='Select1_Changed();'>
			<option value='11'>Lokal</option>
			<option value='22'>Export</option>
			<option value='33'>Retur</option>
			</select>
		</td>
	</tr>
	<tr>
		<td width='15%'>No Bukti</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_kode' id='txt_kode'  size='15' class='input_header' readonly></td>
		<td>Tanggal</td>
		<td>:</td>
		<td><input type='text' name='txt_tgl_jual' id='txt_tgl_jual'  size='10' class='input' readonly></td>

	</tr>
		<tr>
		<td width='15%'>No Kontrak</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_kontrak' id='txt_kontrak'  size='15' class='input' ></td>
		<td>Tgl Kontrak</td>
		<td>:</td>
		<td><input type='text' name='txt_tgl_kontrak' id='txt_tgl_kontrak'  size='15' class='input' readonly></td>
	</tr>
	</tr>
		<tr>
		<td width='15%'>No Pabean</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_pabean' id='txt_pabean'  size='15' class='input' ></td>
		<td>Tgl Pabean</td>
		<td>:</td>
		<td><input type='text' name='txt_tgl_pabean' id='txt_tgl_pabean'  size='15' class='input' readonly ></td>
	</tr>
	<tr>
		<td>Customer</td>
		<td>:</td>
		<td><select name='txt_customer' id='txt_customer' class='input'>
			<option value='' selected>-Pilih-</option>";
		$sql	= "SELECT kode as id_cus, nama as nm_cus FROM customer order by nama ASC";
		$query	= mysql_query($sql);
		while($r=mysql_fetch_array($query)){
			echo "<option value='$r[id_cus]'>$r[id_cus] - $r[nm_cus]</option>";
		}
		echo "
		</select>
		</td>
	</tr>
		<td>Dari Gudang</td>
		<td>:</td>
		<td><select name='txt_gudang' id='txt_gudang' class='input'>
			<option value='' selected>-Pilih-</option>";
		$sql	= "SELECT * FROM gudang ";
		$query	= mysql_query($sql);
		while($r=mysql_fetch_array($query)){
			echo "<option value='$r[id_jenis]'>$r[nm_jenis]</option>";
		}
		echo "
		</select>
		</td>	
	</tr>	
	</tr>
		<td>Jenis Dokumen</td>
		<td>:</td>
		<td><select name='txt_jenis_dokumen' id='txt_jenis_dokumen' class='input'>
			<option value='' selected>-Pilih-</option>";
		$sql	= "SELECT * FROM jenis_dokumen ";
		$query	= mysql_query($sql);
		while($r=mysql_fetch_array($query)){
			echo "<option value='$r[id_jenisdok]'>$r[nm_jenisdok]</option>";
		}
		echo "
		</select>
		</td>	
		<td width='15%'>Keterangan</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_keterangan' id='txt_keterangan'  size='30' class='input' ></td>
	
		</tr>
	</table>";
echo "<table width='100%'>
		<tr>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Kelompok</th>
			<th>Satuan</th>
			<th>Cur</th>
			<th>Jumlah</th>
			<th>Harga Jual</th>
			<th>Total</th>
		<tr>
		<tr>
			<td align='center'><input type='text' name='txt_kode_barang' id='txt_kode_barang' class='input_detail' size='12' maxlength='20'>
			<img src='images/view.png' width='18' height='18' title='Cari Barang' id='list_barang'>
			</td>
			<td align='center'><input type='text' name='txt_nama_barang' id='txt_nama_barang' class='input_detail' size='30' readonly></td>
			<td align='center'><input type='text' name='txt_kelompok' id='txt_kelompok' class='input_detail' size='15' readonly></td>
			<td align='center'><input type='text' name='txt_satuan' id='txt_satuan' class='input_detail' size='5' readonly></td>
			<td><select name='txt_cur' id='txt_cur' class='input'>
			<option value='RP'>RP</option>
			<option value='USD'>USD</option>
			<option value='YEN'>YEN</option>
			</select>
			</td>
			<td align='center'><input type='text' name='txt_jumlah' id='txt_jumlah' class='input_detail' size='5' maxlength='10'></td>
			<td align='center'><input type='text' name='txt_harga' id='txt_harga' class='input_detail' size='8' readonly></td>
			<td align='center'><input type='text' name='txt_total' id='txt_total' class='input_detail' size='12'></td>
		</tr>
	</table>";
echo "<table width='100%'>
	<tr>
		<td colspan='3' align='center'>
		<button name='tambah_barang' id='tambah_barang'>Tambah BARANG</button>
		<button name='simpan' id='simpan'>Simpan BARANG</button>
		<button name='tambah' id='tambah'>Tambah Transaksi</button> ";
		if ($lv=="superadmin" or $lv=="admin" ){
			echo "<button name='edit' id='edit'>Edit</button> ";
			echo "<button name='hapus_bukti' id='hapus_bukti'>Hapus Bukti</button> ";
		}
		echo "
		<!------<button name='edit' id='edit'>Edit</button>
		<button name='hapus_bukti' id='hapus_bukti'>Hapus Bukti</button>------>
		</td>
	</tr>
	</table>";
echo "<div id='data_detail'></div>";
echo "<table width='100%'>
	<tr>
		<td colspan='3' align='center'>

		</td>
	</tr>
	</table>";
echo "<div id='form_cari_barang' title='Pencarian Barang'>
		<table width='100%'>
			<td>
				<td width='15%'>Cari Barang</td>
				<td width='2%'>:</td>
				<td><input type='text' name='txt_cari' id='txt_cari' size='50''</td>
			</td>
		</table>
		<div id='info_barang' align='center'></div>
	</div>";
			
?>