<?
session_start();
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#txt_kode_barang").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kode").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_keterangan").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kontrak").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kontrak").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
						   
	$("#txt_tgl_jual").datepicker({
		dateFormat      : "dd-mm-yy",        
	  	showOn          : "button",
	  	buttonImage     : "images/calendar.gif",
	  	buttonImageOnly : true				
	});
/* 	$("#txt_jumlah").keypress(function(data){
		if (data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) 
		{
			return false;
		}
	});
 */
 
 	 /* angka desimal yang dapat dientry */
$		('#txt_jumlah').keypress(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	/* hanya angka yang dapat dientry */
	$("#txt_total").keypress(function(data){
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) &&   ($(this).val().substring($(this).val().indexOf('.'),$(this).val().indexOf('.').length).length>2 )){
            event.preventDefault();
        }
	});
	
	
	$('#form_cari_barang').dialog({
		autoOpen: false,
		modal	: true,
		width: 600,
	});
	
	// Dialog Link
	$('#list_barang').click(function(){
		$('#form_cari_barang').dialog('open');
		return false;
	});
	cari_nomor();
	
	function cari_nomor(){
		var no = 1;
		$.ajax({
			type	: "POST",
			url		: "modul/mutasi_hasil/cari_nomor.php",
			data	: "no="+no,
			dataType: "json",
			success	: function(data){
				$("#txt_kode").val(data.nomor);
				tampil_data_detail();
			}
		});
	}
	
	function cari_bukti() {
		var kode	= $("#txt_kode").val();
		$.ajax({
			type	: "POST",
			url		: "modul/mutasi_hasil/cari_kode.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_tgl_jual").val(data.tgl);
				$("#txt_kontrak").val(data.no_kontrak);
				$("#txt_customer").val(data.id_supplier);
				$("#txt_gudang").val(data.gudang);
				$("#txt_gudang1").val(data.gudang1);
				$("#txt_jenis_pekerjaan").val(data.id_jenis);
				$("#txt_keterangan").val(data.keterangan);
				tampil_data_detail();
			}
		});		
	}
	
	$("#txt_kode").keyup(function() {
		cari_bukti();
	});
	$("#txt_kode").focus(function() {
		cari_bukti();
	});
	
	$("#txt_kontrak").autocomplete("modul/penjualan/list_kontrak.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#txt_kode").autocomplete("modul/mutasi_hasil/list_bukti.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	function tampil_data_detail(){
		var kode = $("#txt_kode").val();
		$("#data_detail").load("modul/mutasi_hasil/tampil_data_detail.php?kode="+kode);
	}
	
	$("#tambah_barang").click(function(){
		$(".input_detail").val('');
		$("#txt_kode_barang").focus();
	});
	
	$("#txt_kode_barang").keyup(function() {
		var kode 	= $("#txt_kode_barang").val()
		//var akses	= 1; //1 cari, 2 edit, 3 baru
		$.ajax({
			type	: "POST",
			url		: "modul/mutasi_hasil/cari_barang.php",
			data	: "kode="+kode,
			dataType	: "json",
			success	: function(data){
				$("#txt_nama_barang").val(data.nama_barang);
				$("#txt_kelompok").val(data.kelompok);
				$("#txt_satuan").val(data.satuan);
				$("#txt_harga").val(data.harga);
			}
		});
	});

	$("#txt_jumlah").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var harga	= $("#txt_harga").val();
		if (jml.length!='') {
			var total	= parseInt(jml)*parseInt(harga);
			$("#txt_total").val(total);
		}else{
			$("#txt_total").val(0);
		}
	});

	$("#simpan").click(function(){
		simpan_data();
	});

	function simpan_data(){
		var	kode		= $("#txt_kode").val();
		var	tgl			= $("#txt_tgl_jual").val();
		var	kode_brg	= $("#txt_kode_barang").val();
		var	nama_brg	= $("#txt_nama_barang").val();
		var	jumlah		= $("#txt_jumlah").val();
		var	harga		= $("#txt_harga").val();
		var satuan		= $("#txt_satuan").val();
		var no_kontrak	= $("#txt_kontrak").val();
		var id_jenis	= $("#txt_jenis_pekerjaan").val();
		var kelompok	= $("#txt_kelompok").val();
		var keterangan	= $("#txt_keterangan").val();
		var gudang		= $("#txt_gudang").val();
		var gudang1		= $("#txt_gudang1").val();
		
		var error = false;

		if(kode.length == 0){
           var error = true;
           alert("Maaf, Kode mutasi_hasil tidak boleh kosong");
		   $("#txt_kode").focus();
		   return (false);
         }
		if(tgl.length == 0){
           var error = true;
           alert("Maaf, Tanggal tidak boleh kosong");
		   //$("#txt_nama").focus();
		   return (false);
         }	 
		if(kode_brg.length == 0){
           var error = true;
           alert("Maaf, Kode Barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(nama_brg.length == 0){
           var error = true;
           alert("Maaf, Nama Barang tidak boleh kosong");
		   $("#txt_nama_barang").focus();
		   return (false);
         }
		if(jumlah.length == 0){
           var error = true;
           alert("Maaf, Jumlah Barang tidak boleh kosong");
		   $("#txt_jumlah").focus();
		   return (false);
         }
		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/mutasi_hasil/simpan.php",
			data	: "kode="+kode+
					"&tgl="+tgl+
					"&kode_brg="+kode_brg+
					"&nama_brg="+nama_brg+
					"&satuan="+satuan+
					"&jumlah="+jumlah+
					"&kelompok="+kelompok+
					"&no_kontrak="+no_kontrak+
					"&id_jenis="+id_jenis+
					"&gudang="+gudang+
					"&gudang1="+gudang1+
					"&harga="+harga+
					"&keterangan="+keterangan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info").html(data);
				tampil_data_detail();
			}
		});
		}
		return false; 		
	}
	
	$("#tambah").click(function(){
		cari_nomor();
		$(".input_detail").val('');
		$(".input").val('');
		$(".input").removeAttr("readonly");
		$("#txt_tgl_jual").val('');
		$("#txt_tgl_jual").focus();
	});

	$("#edit").click(function() {
		$("#txt_kode").removeAttr("readonly");
		$('.input').attr('readonly','true');
		$(".input_detail").val('');
		$(".input").val('');
		$('#txt_kode').val('');
		$("#txt_kode").focus();
	});
	
	$("#hapus_bukti").click(function() {
		var pilih = confirm('Bukti ini akan dihapus ?');
		if (pilih==true) {
		var kode	= $("#txt_kode").val();
		$.ajax({
			type	: "POST",
			url		: "modul/mutasi_hasil/hapus_bukti.php",
			data	: "kode="+kode,
			success	: function(data){
				$("#info").html(data);
					tambah();
			}
		});
		}
	});
	
	$("#txt_cari").keyup(function(){
		var cari	= $("#txt_cari").val();								  
		$.ajax({
			type	: "POST",
			url		: "modul/mutasi_hasil/tampil_data_barang.php",
			data	: "cari="+cari,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info_barang").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info_barang").html(data);
			}
		});
	});

	
});
</script>
<style type="text/css">
#txt_kode {
	background:#CCC;
}
#list_barang{
	cursor:pointer;
}
.input_detail readonly {
	background:#666;
}
</style>
<?php

function generateRandomString($length = 50) {
						return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
					}
					$gen=generateRandomString();
					$id = rand (0,50);
					
?>

<?php
include '../../inc/inc.koneksi.php';

$row = $_SESSION['namauser'];
$lv	= $_SESSION['leveluser'];


echo "<table width='100%'>
	<tr>
		<td width='15%'>No Bukti</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_kode' id='txt_kode'  size='15' class='input_header' readonly></td>
		<td>Tanggal</td>
		<td>:</td>
		<td><input type='text' name='txt_tgl_jual' id='txt_tgl_jual'  size='10' class='input_header' readonly></td>
	</tr>
	<tr>
		<td width='15%'>No Kontrak</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_kontrak' id='txt_kontrak'  size='15' class='input' ></td>
	</tr>
	<tr>
		<td>Dari Gudang</td>
		<td>:</td>
		<td><select name='txt_gudang' id='txt_gudang' class='input'>
			<option value='' selected>-Pilih-</option>";
		$sql	= "SELECT * FROM gudang ";
		$query	= mysql_query($sql);
		while($r=mysql_fetch_array($query)){
			echo "<option value='$r[id_jenis]'>$r[nm_jenis]</option>";
		}
		echo "
		</select>
		</td>	
		<td>Ke Gudang</td>
		<td>:</td>
		<td><select name='txt_gudang1' id='txt_gudang1' class='input'>
			<option value='' selected>-Pilih-</option>";
		$sql	= "SELECT * FROM gudang ";
		$query	= mysql_query($sql);
		while($r=mysql_fetch_array($query)){
			echo "<option value='$r[id_jenis]'>$r[nm_jenis]</option>";
		}
		echo "
		</select>
		</td>	
	</tr>
		<td>Jenis Pekerjaan</td>
		<td>:</td>
		<td><select name='txt_jenis_pekerjaan' id='txt_jenis_pekerjaan' class='input'>
			<option value='' selected>-Pilih-</option>";
		$sql	= "SELECT * FROM jenis_barang ";
		$query	= mysql_query($sql);
		while($r=mysql_fetch_array($query)){
			echo "<option value='$r[id_jenis]'>$r[nm_jenis]</option>";
		}
		echo "
		</select>
		</td>	
		<td width='15%'>Keterangan</td>
		<td width='2%'>:</td>
		<td><input type='text' name='txt_keterangan' id='txt_keterangan'  size='30' class='input' ></td>
	
		</tr>
	</table>";
echo "<table width='100%'>
		<tr>
			<th>Kode Barang</th>
			<th>Nama Barang</th>
			<th>Kelompok</th>
			<th>Satuan</th>
			<th>Jumlah</th>
			<th>Harga Jual</th>
			<th>Total</th>
		<tr>
		<tr>
			<td align='center'><input type='text' name='txt_kode_barang' id='txt_kode_barang' class='input_detail' size='12' maxlength='20' readonly>
			<img src='images/view.png' width='18' height='18' title='Cari Barang' id='list_barang'>
			</td>
			<td align='center'><input type='text' name='txt_nama_barang' id='txt_nama_barang' class='input_detail' size='35' readonly></td>
			<td align='center'><input type='text' name='txt_kelompok' id='txt_kelompok' class='input_detail' size='20' readonly></td>
			<td align='center'><input type='text' name='txt_satuan' id='txt_satuan' class='input_detail' size='5' readonly></td>
			<td align='center'><input type='text' name='txt_jumlah' id='txt_jumlah' class='input_detail' size='5' maxlength='10'></td>
			<td align='center'><input type='text' name='txt_harga' id='txt_harga' class='input_detail' size='8' readonly></td>
			<td align='center'><input type='text' name='txt_total' id='txt_total' class='input_detail' size='12' readonly></td>
		</tr>
	</table>";
echo "<table width='100%'>
	<tr>
		<td colspan='3' align='center'>
		<button name='tambah_barang' id='tambah_barang'>Tambah BARANG</button>
		<button name='simpan' id='simpan'>Simpan BARANG</button>
		<button name='tambah' id='tambah'>Tambah Transaksi</button> ";
		if ($lv=="superadmin" or $lv=="admin" ){
			echo "<button name='edit' id='edit'>Edit</button> ";
			echo "<button name='hapus_bukti' id='hapus_bukti'>Hapus Bukti</button> ";
		}
		echo "
		<!-----<button name='edit' id='edit'>Edit</button>
		<button name='hapus_bukti' id='hapus_bukti'>Hapus Bukti</button>----->
		<button onclick='myFunction()'>Cetak</button>

<script>
function myFunction() {
    window.open('mutasi_hasil2.php?is=$gen?id=$id','Mutasi Hasil','width=900,height=600,scrollbars=yes,resizeable=no');
}
</script>
		</td>
	</tr>
	</table>";
echo "<div id='data_detail'></div>";
echo "<table width='100%'>
	<tr>
		<td colspan='3' align='center'>

		</td>
	</tr>
	</table>";
echo "<div id='form_cari_barang' title='Pencarian Barang'>
		<table width='100%'>
			<td>
				<td width='15%'>Cari Barang</td>
				<td width='2%'>:</td>
				<td><input type='text' name='txt_cari' id='txt_cari' size='50''</td>
			</td>
		</table>
		<div id='info_barang' align='center'></div>
	</div>";
			
?>