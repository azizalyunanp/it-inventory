// JavaScript Document
$(document).ready(function() {
	//membuat text kode barang menjadi Kapital
	$("#kode_barang").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_no_kontrak").focus(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_no_kontrak").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});
	
	$("#txt_kode_beli").keyup(function(e){
		var isi = $(e.target).val();
		$(e.target).val(isi.toUpperCase());
	});

	// format datepicker untuk tanggal
	$("#txt_tgl_beli").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	$("#txt_tgl_invoice").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
		
	$("#txt_tgl_pabean").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
			
	$("#txt_tgl_skep").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});

	$("#txt_tgl_packing").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
			
	$("#txt_tgl_suratjalan").datepicker({
				dateFormat      : "dd-mm-yy",        
	  showOn          : "button",
	  buttonImage     : "images/calendar.gif",
	  buttonImageOnly : true				
	});
	
	//hanya angka yang dapat dientry
	$("#txt_jumlah").keypress(function(data){
		if (data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)) 
		{
			return false;
		}
	});


	function kosong(){
		$(".detail_readonly").val('');
		$(".input_detail").val('');
	}
	
	function cari_nomor() {
		var no		=1;
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/cari_nomor.php",
			data	: "no="+no,
			dataType : "json",
			success	: function(data){
				$("#txt_kode_beli").val(data.nomor);
				tampil_data();
			}
		});		
	}

	function tampil_data() {
		var kode 	= $("#txt_kode_beli").val();;
		$.ajax({
				type	: "POST",
				url		: "modul/stpnb/tampil_data.php",
				data	: "kode="+kode,
				timeout	: 3000,
				beforeSend	: function(){		
					$("#info").html("<img src='loading.gif' />");			
				},				  
				success	: function(data){
					$("#info").html(data);
				}
		});			
	}

	
	cari_nomor();
	
	$("#txt_kode_barang").autocomplete("modul/stpnb/list_barang.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#txt_no_kontrak").autocomplete("modul/stpnb/list_kontrak.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	$("#txt_kode_beli").autocomplete("modul/stpnb/list_bukti.php", {
				width:100,
				max: 10,
				scroll:false,
	});
	
	function cari_kode() {
		var kode	= $("#txt_kode_barang").val();
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/cari_barang.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_nama_barang").val(data.nama_barang);
				$("#txt_satuan").val(data.satuan);
				$("#txt_kelompok").val(data.kelompok);
				$("#txt_harga").val(data.harga);
			}
		});		
	}
	
	function cari_bukti() {
		var kode	= $("#txt_kode_beli").val();
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/cari_kode.php",
			data	: "kode="+kode,
			dataType : "json",
			success	: function(data){
				$("#txt_tgl_beli").val(data.tgl);
				$("#txt_tgl_invoice").val(data.tgl_invoice);
				$("#txt_tgl_pabean").val(data.tgl_pabean);
				$("#txt_tgl_packing").val(data.tgl_packing);
				$("#txt_tgl_skep").val(data.tgl_skep);
				$("#txt_tgl_suratjalan").val(data.tgl_suratjalan);
				$("#txt_jenis_dokumen").val(data.id_jenisdok);
				$("#txt_no_kontrak").val(data.no_kontrak);
				$("#txt_no_invoice").val(data.no_invoice);
				$("#txt_no_pabean").val(data.no_pabean);
				$("#txt_no_packing").val(data.no_packing);
				$("#txt_no_skep").val(data.no_skep);
				$("#txt_no_suratjalan").val(data.no_suratjalan);
				$("#cbo_supplier").val(data.id_supplier);
				$("#txt_gudang").val(data.gudang);
				$("#txt_keterangan").val(data.keterangan);
				tampil_data();
			}
		});		
	}
	
	
	$("#txt_kode_barang").keyup(function() {
		cari_kode();
	});
	$("#txt_kode_barang").focus(function() {
		cari_kode();
	});
	
	$("#txt_no_kontrak").keyup(function() {
		cari_kode();
	});
	$("#txt_no_kontrak").focus(function() {
		cari_kode();
	});
	
	$("#txt_kode_beli").keyup(function() {
		cari_bukti();
	});
	$("#txt_kode_beli").focus(function() {
		cari_bukti();
	});
	
	//mengalikan jumlah dengan harga
	$("#txt_jumlah").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var harga	= $("#txt_harga").val();
		if (jml.length!='') {
			var total	= parseInt(jml)*harga;
			$("#txt_total").val(total);
		}else{
			$("#txt_total").val(0);
		}
	});
	
	//membagi harga total
	$("#txt_total").keyup(function(){
		var jml		= $("#txt_jumlah").val();
		var total	= $("#txt_total").val();
		if (jml.length!='') {
			var harga	= total/parseInt(jml);
			$("#txt_harga").val(harga);
		}
	});


	$("#tambah_detail").click(function(){
		kosong();	
		$("#txt_kode_barang").focus();
	});

	
	$("#simpan").click(function(){
		var kode			= $("#txt_kode_beli").val();	
		var tgl				= $("#txt_tgl_beli").val();	
		var supplier		= $("#cbo_supplier").val();	
		var	kode_barang		= $("#txt_kode_barang").val();
		var	nama_barang		= $("#txt_nama_barang").val();
		var	satuan			= $("#txt_satuan").val();
		var	jumlah			= $("#txt_jumlah").val();
		var harga			= $("#txt_harga").val();
		var kelompok		= $("#txt_kelompok").val();
		var total			= $("#txt_total").val();
		var jenisdok		= $("#txt_jenis_dokumen").val();
		var	tgl_invoice		= $("#txt_tgl_invoice").val();
		var	tgl_pabean		= $("#txt_tgl_pabean").val();
		var	tgl_skep		= $("#txt_tgl_skep").val();
		var	tgl_packing		= $("#txt_tgl_packing").val();
		var	tgl_suratjalan	= $("#txt_tgl_suratjalan").val();
		var	invoice			= $("#txt_no_invoice").val();
		var	pabean			= $("#txt_no_pabean").val();
		var	skep			= $("#txt_no_skep").val();
		var	packing			= $("#txt_no_packing").val();
		var	suratjalan		= $("#txt_no_suratjalan").val();
		var kontrak 		= $("#txt_no_kontrak").val();
		var keterangan		= $("#txt_keterangan").val();
		var gudang			= $("#txt_gudang").val();
		var cur			= $("#txt_cur").val();
		
		var error = false;
		if(kode.length == 0){
           var error = true;
           alert("Maaf, Kode Pembelian tidak boleh kosong");
		   $("#txt_kode_beli").focus();
		   return (false);
         }
		if(tgl.length == 0){
           var error = true;
           alert("Maaf, Tanggal Bukti tidak boleh kosong");
		   $("#txt_tgl_beli").focus();
		   return (false);
         }
		 
		 		 
		if(supplier.length == 0){
           var error = true;
           alert("Maaf, Supplier tidak boleh kosong");
		   $("#cbo_supplier").focus();
		   return (false);
         }
		 
		 if(jenisdok.length == 0){
           var error = true;
           alert("Maaf, Jenis Dokumen tidak boleh kosong");
		   $("#txt_jenis_dokumen").focus();
		   return (false);
         }
		 
		if(pabean.length == 0){
           var error = true;
           alert("Maaf, No Pabean tidak boleh kosong");
		   $("#txt_no_pabean").focus();
		   return (false);
         }
		 
		if(tgl_pabean.length == 0){
           var error = true;
           alert("Maaf, Tanggal pabean tidak boleh kosong");
		   $("#txt_tgl_pabean").focus();
		   return (false);
         }
		
		if(kontrak.length == 0){
           var error = true;
           alert("Maaf, Nomor Kontrak tidak boleh kosong");
		   $("#txt_no_kontrak").focus();
		   return (false);
         }
		 
		if(gudang.length == 0){
           var error = true;
           alert("Maaf, Gudang tidak boleh kosong");
		   $("#txt_gudang").focus();
		   return (false);
         }

		if(kode_barang.length == 0){
           var error = true;
           alert("Maaf, Kode barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		 
		if(nama_barang.length == 0){
           var error = true;
           alert("Maaf, Nama Barang tidak boleh kosong");
		   $("#txt_kode_barang").focus();
		   return (false);
         }
		if(jumlah.length == 0){
           var error = true;
           alert("Maaf, Jumlah Barang tidak boleh kosong");
		   $("#txt_jumlah").focus();
		   return (false);
         }

		 		 
		if(error == false){
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/simpan.php",
			data	: "kode="+kode+
						"&tgl="+tgl+
						"&supplier="+supplier+
						"&kode_barang="+kode_barang+
						"&nama_barang="+nama_barang+
						"&kelompok="+kelompok+
						"&satuan="+satuan+
						"&cur="+cur+
						"&jumlah="+jumlah+
						"&harga="+harga+
						"&total="+total+
						"&jenisdok="+jenisdok+
						"&invoice="+invoice+
						"&tgl_invoice="+tgl_invoice+
						"&pabean="+pabean+
						"&tgl_pabean="+tgl_pabean+
						"&skep="+skep+
						"&tgl_skep="+tgl_skep+
						"&packing="+packing+
						"&tgl_packing="+tgl_packing+
						"&suratjalan="+suratjalan+
						"&tgl_suratjalan="+tgl_suratjalan+
						"&kontrak="+kontrak+
						"&gudang="+gudang+
						"&keterangan="+keterangan,
			timeout	: 3000,
			beforeSend	: function(){		
				$("#info").show();
				$("#info").html("<img src='loading.gif' />");			
			},				  
			success	: function(data){
				$("#info").show();
				$("#info").html(data);
			}
		});
		}
		return false; 
	});

	$("#tambah_beli").click(function() {
		$(".input").val('');
		$(".input").removeAttr("readonly");
		$('#txt_kode_beli').attr('readonly','true');
		kosong();
		cari_nomor();
		$("#txt_tgl_beli").focus();
	});
	
	$("#hapus_bukti").click(function() {
		var pilih = confirm('Bukti ini akan dihapus ?');
		if (pilih==true) {
		var kode	= $("#txt_kode_beli").val();
		$.ajax({
			type	: "POST",
			url		: "modul/stpnb/hapus_bukti.php",
			data	: "kode="+kode,
			success	: function(data){
				$("#info").html(data);
				$(".detail_readonly").val('');
				$(".input_detail").val('');
			}
		});
		}
	});
		
	$("#edit").click(function() {
		$("#txt_kode_beli").removeAttr("readonly");
		$('.input').attr('readonly','true');
		$('#txt_kode_beli').val('');
		$("#txt_kode_beli").focus();
	});


	$("#keluar").click(function(){
		window.close();
	});

});

