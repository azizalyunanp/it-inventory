<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Panduan</title>
<div id="title" font-size="14"><strong>
PT. SARI WARNA ASLI <br/>
Unit Garment<br/></strong>
JL. Hos Cokroaminoto no. 28<br/>
Website: <a href="http://swagarment.com" target="_blank">http://swagarment.com</a>
<br/>

<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="../../jquery_easyui/themes/style.css" />
<script type="text/javascript" src="../../jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="../../jquery_easyui/jquery.easyui.min.js"></script>

</head>
<body style=" background-repeat:no-repeat;background-color:#ebebeb;">
<a style="display:scroll;position:fixed;bottom:5px;right:5px;text-align:center;font-family:Comic Sans MS;font-size:13px;" href="#" title="Back to Top">
<b>Back to<br/> TOP MENU</b></a>
<br clear="all" />
<strong> Daftar Panduan: ( Klik pada nomor untuk langsung menuju ke halaman ) </strong><span style="font-size: large;"><b>&nbsp; 
<ul>
<li><a href="#pengisiandatabarang">1. Pengisian Data Barang</a></li>
<li><a href="#transaksikontrak">2. Pengisian Input Kontrak</a></li>
<li><a href="#barangmasuk">3. Pengisian Barang Masuk</a></li>
<li><a href="#barangproses">4. Pengisian Mutasi Proses</a></li>
<li><a href="#dlmproses">5. Pengisian Barang Dalam Proses / WIP</a></li>
<li><a href="#barangjadi">6. Pengisian Mutasi Barang Jadi /  Hasil</a></li>
<li><a href="#barangkeluar">7. Pengisian Barang Keluar</a></li>
 </ul>
 </b>
 </span>
<a href="" name="pengisiandatabarang"></a><b>1. PENGISIAN DATA BARANG</b><br />
Dalam pengisian Data Barang perlu di perhatikan dalam pengkodean dan kelompok barang yang akan dimasukkan.<br/>
Gunanya untuk mempermudah dalam pengindexan data barang<br/><br/>
<img border="0" src="help/databarang1.jpg" alt="databarang" width="auto" height="auto" /><br/>
 Contoh: <br/>
- Perhatikan dalam kelompok barang tertulis 01/Accesories. Maka 2 digit pengkodean barang ditulis [01] ini index kelompok barang<br/><br/>
<img border="0" src="help/databarang3.jpg" alt="databarang" width="auto" height="auto" /><br/>
- 4 digit setelah kelompok barang adalah nomor kontrak : contoh diatas [0144] ini index kontrak misalkan berdasar no kontrak 144/BC/2014 jika no kontrak hanya 144/BC/2014 jadikan 0144 dalam penulisan kode barang. <br/>
- 2 digit berikutnya adalah index supplier/customer yang misal 02 itu barang dr customer A. yg lain 05 itu customer B.<br/>
- 2 digit berikutnya adalah index urutan barang.<br/><br/>
<img border="0" src="help/databarang2.jpg" alt="databarang" width="auto" height="auto" /><br/>
- Pengisian harga beli dan harga jual. adalah harga per satuan barang.<br/>
- Pastikan stok di isi dengan 0.<br/>
- Keterangan di isi dengan keterangan barang. <br>
- Pastikan semua form terisi saat pengisian sebelum di simpan.
<br />
<br />
<a href="" name="transaksikontrak"></a><b>2. PENGISIAN INPUT KONTRAK</b><br />
Transaksi Input Kontrak adalah pengisian 1 bukti kontrak, semua list barang dalam 1 kontrak dan jumlah total dari barang tersebut.<br/>
Gunanya untuk pengindexan list barang berdasar nomor kontrak dari barang masuk, proses sampai barang keluar. <br/>
<b>(WAJIB DI BUAT SEBELUM MELAKUKAN PROSES INPUT TRANSAKSI YG LAIN) </b><br/><br/>
<img border="0" src="help/listkontrak1.jpg" alt="masuk" width="800" height="auto" /><br/>
- Pengisian dapat dilakukan dengan menekan tombol tambah transaksi.<br/>
- Isi sesuai data yang di minta dan pastikan semua data terisi.<br/>
- Nomor kontrak <b>[WAJIB DI ISI, BAIK DENGAN NOMOR KONTRAK EXTERN ATAU INTERN]</b><br/><br/>
<img border="0" src="help/listkontrak2.jpg" alt="masuk" width="800" height="auto" /><br/>
- Untuk menyimpan klik simpan barang. dan klik tambah barang untuk menambah list barang.<br/>
- Pastikan kode barang dan nomor kontrak sama. dari contoh di atas barang 0101440204 itu untuk nomor kontrak 0144/BC/2014<br/>
- Untuk mengakhiri klik tambah transaksi atau tutup tab input kontrak.<br/>
<br/>
<a href="" name="barangmasuk"></a><b>3. PENGISIAN BARANG MASUK</b><br />
Transaksi Input Barang Masuk adalah bukti transaksi setiap kali barang datang dari supplier atau customer yang berdasarkan pada nomor kontrak. <b>[LIST BARANG HARUS SUDAH DI BUAT PADA <a href="#transaksikontrak">ORDER KONTRAK</a>]</b><br/>
Barang masuk mempengaruhi stok barang datang yang nantinya akan diproses.<br/><br/>
<img border="0" src="help/barang masuk.jpg" alt="brgmasuk" width="800" height="auto" /><br/>
- Tekan tombol tambah transaksi dalam setiap penginputan barang masuk baru.<br/><br/>
<img border="0" src="help/isi barang masuk.jpg" alt="brgmasuk" width="800" height="auto" /><br/>
- Isi data sesuai yang form yang di butuhkan, berdasar data manual kontrak.<br/>
- Nomor kontrak dapat dipanggil jika order kontrak sudah di buat <b>[JANGAN DI INPUT MANUAL, PASTIKAN NOMOR KONTRAK SUDAH TERDAFTAR DI <a href="#transaksikontrak">ORDER KONTRAK</a>]</b>, <br/>
- NOMOR PABEAN DAN TANGGAL PABEAN WAJIB DI ISI
- Jika nomor selain nomor kontrak yang memang tidak ada nomor yg bersangkutan, di isi dengan minus(-).<br/>
- Jika semuanya sudah terisi pastikan kode baranga sesuai dengan nomor kontrak. untuk mempermudah pengindexan.<br/>
- Jika salah, hapus bukti dan ulangi lagi.<br/><br/>
<a href="" name="editdata"></a><img border="0" src="help/edit barang masuk.jpg" alt="brgmasuk" width="800" height="auto" /><br/>
- Untuk edit data. tekan tombol edit lalu isi dengan nomor bukti yang akan di edit. untuk bagian form tidak dapat di edit.<br/>
  yang dapat di edit hanya isi barang dengan cara menghapus barang yang salah, lalu di tambah barang lagi.
<br/><br/>
<a href="" name="barangproses"></a><b>4. PENGISIAN MUTASI PROSES</b><br />
SEDANG DALAM PROSES
<br/><br/><br/><br/>
<a href="" name="dlmproses"></a><b>5. PENGISIAN BARANG DALAM PROSES / WIP</b><br />
SEDANG DALAM PROSES
<br/><br/><br/><br/>
<a href="" name="barangjadi"></a><b>6. PENGISIAN MUTASI BARANG JADI/ HASIL</b><br />
Transaksi ini berdasarkan manual hasil produksi. ini mempengaruhi stok dengan jenis barang yang berbeda tetapi tetap berdasarkan nomor kontrak.<br/>
<b>[PASTIKAN BARANG YANG AKAN DI KIRIM PADA BAGIAN NOMOR KONTRAK NYA SUDAH ADA DI <a href="#transaksikontrak">ORDER KONTRAK</b></a>]<br/><br/>
<img border="0" src="help/input mutasi hasil.jpg" alt="hasil" width="800" height="auto" /><br/>
- Semua wajib di isi.<br/>
<img border="0" src="help/isi mutasi hasil.jpg" alt="hasil" width="800" height="auto" /><br/>
- Pastikan kode barang sesuai dengan nomor kontrak, pada contoh di atas kode barang 0801560712 index 0156 untuk nomor kontrak 156/EML/07/2014<br/>
- Untuk edit tutorial sama dengan <a href="#editdata">edit barang masuk</a><br/>
<br/>
<br/>
<a href="" name="barangkeluar"></a><b>5. PENGISIAN BARANG KELUAR</b><br />
Transaksi ini berdasarkan bukti pengiriman manual. mempengaruhi stok barang jadi. <br />
Jika mutasi hasil belum di input. stok tidak dapat di ambil. ada peringatan stok 0.<br />
<b>[PASTIKAN BARANG YANG AKAN DI KIRIM PADA BAGIAN NOMOR KONTRAK NYA SUDAH ADA DI <a href="#transaksikontrak">ORDER KONTRAK</b></a>]<br/><br/>
<img border="0" src="help/input barang keluar.jpg" alt="keluar" width="800" height="auto" /><br/>
- Cara input sama, semua wajib di isi.<br />
- Nomor kontrak di panggil berdasar list di order kontrak.<br />
- NOMOR PABEAN DAN TANGGAL PABEAN WAJIB DI ISI.
<img border="0" src="help/isi barang keluar.jpg" alt="keluar" width="800" height="auto" /><br/>
- Pastikan semua data di isi. keterangan di perlukan.<br />
- Pastikan kode barang sesuai dengan nomor kontrak, pada contoh di atas kode barang 0801560712 index 0156 untuk nomor kontrak 156/EML/07/2014<br/><br />
<img border="0" src="help/isi daftar barang keluar.jpg" alt="keluar" width="800" height="auto" /><br/>
- Untuk melihat daftar barang keluar. dapat di lihat dengan filter pencarian hari.<br />

<br/>

</body>
</html>
