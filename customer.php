<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/style.css">
<script type="text/javascript" src="jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="jquery_easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="libs_js/customer.js"></script>
</head>

<body>
<h2>OLAH DATA CUSTOMER</h2>
<div class="info" style="margin-bottom:10px">
		<div class="tip icon-tip">&nbsp;</div>
		<div>Klik tombol pada datagrid toolbar untuk melakukan perubahan data.</div>
	</div>
	
	<table id="dg" title="DATA BUYER" class="easyui-datagrid" style="height:350px"
			url="data_master/customer/get.php"
			toolbar="#toolbar" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
            	<th field="kode" width="37">ID CUSTOMER</th>
				<th field="nama" width="50">NAMA CUSTOMER</th>
				<th field="alamat" width="85">ALAMAT CUSTOMER</th>
				<th field="kota" width="40">KOTA</th>
				<th field="telpon" width="50">TELEPON</th>
				<th field="fax" width="35">FAX</th>
				<th field="email" width="70">EMAIL</th>
			</tr>
		</thead>
	</table>
	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newData()">Data Baru</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editData()">Edit Data</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeData()">Hapus Data</a>
	</div>
    
    <div id="dlg" class="easyui-dialog" style="width:400px;height:310px;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Informasi Buyer</div>
		<form id="fm" method="post" novalidate>
			<div class="fitem">
				<label>Id Customer:</label>
			  	<input name="kode" id="kode" class="easyui-validatebox" required="true">
			</div>
		    <div class="fitem">
				<label>Nama Customer:</label>
			  	<input name="nama" width="200" id="nama" class="easyui-validatebox" required="true">
			</div>
            <div class="fitem">
		  	  <label>Alamat Customer:</label>
                <textarea name="alamat" cols="33" rows="3" class="easyui-validatebox" id="alamat" required="true"></textarea>
		  	</div>
			  <div class="fitem">
		  	  <label>Kota:</label>
                <input name="kota" class="easyui-validatebox" id="kota" required="true">
		  	</div>
          <div class="fitem">
			<label>Telepon:</label>
		  	  <input name="telpon"  id="telpon" required="false" >
			</div>
		  <div class="fitem">
			<label>Fax:</label>
		  	  <input name="fax"  id="fax" required="false" >
			</div>
		  <div class="fitem">
			<label>Email:</label>
		  	  <input name="email"  id="email" required="false" validType="email">
			</div>
         </form>
	</div>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveData()">Save</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancel</a>
	</div>
</body>
</html>