<?php
include 'inc/cek_session.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistem Persediaan Barang</title>
<link rel="stylesheet" href="css/icon.css" type="text/css" />
<link rel="stylesheet" href="css/superfish.css" type="text/css" />
<link rel="stylesheet" href="css/style_content.css" type="text/css" />
<link rel="stylesheet" href="css/style_tabel.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="mycss/index.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/panel.css" />

<script type="text/javascript" src="jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="jquery_easyui/jquery.easyui.min.js"></script>


<script>
function addTab(title, url){
			if ($('#tt').tabs('exists', title)){
				$('#tt').tabs('select', title);
			} else {
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
				$('#tt').tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}
		}
	</script>
	
	<script language="javascript">
var win = null;
function NewWindow(mypage,myname,w,h,scroll){
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
settings =
'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
win = window.open(mypage,myname,settings)
}
</script>

<script type="text/javascript" src="js/hoverIntent.js"></script>
<!-- untuk menu superfish -->
<script type="text/javascript" src="js/superfish.js"></script>

<!-- untuk datepicker -->
<link type="text/css" href="css/ui.all.css" rel="stylesheet" />   
<script type="text/javascript" src="js/ui.core.js"></script>
<script type="text/javascript" src="js/ui.datepicker.js"></script>
<script type="text/javascript" src="js/ui.datepicker-id.js"></script>

<!-- untuk autocomplite -->
<link rel="stylesheet" type="text/css" href="js/jquery.autocomplete.css" />
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>

<!-- plugin untuk tab -->
<link type="text/css" href="css/smoothness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	   $('ul.sf-menu').superfish();
  });
</script>
</head>
<body>
<div id="header">
</div>
<div id="navigasi">
<div style="width:200px;height:auto;padding:5px;float:left; margin-right:10px;background:#7190E0;">
<div class="easyui-panel" title="OLAH DATA MASTER" collapsed="true" collapsible="true" style="width:200px;height:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-report4" plain="true" onClick="addTab('Jenis Dokumen','jenis_dokumen.php')">Jenis Dokumen</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-supplier" plain="true" onClick="addTab('Data Customer','customer.php')">Data Customer</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box" plain="true" onClick="addTab('Jenis Barang','jenis_barang.php')">Jenis Pekerjaan</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box" plain="true" onClick="addTab('Kelompok Barang','kelompok_barang.php')">Kelompok Barang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box-fill" plain="true" onClick="addTab('Data Barang','barang.php')">Data Barang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Jenis Mutasi','jenis_mutasi.php')">Jenis Mutasi</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-sum" plain="true" onClick="addTab('Satuan','satuan.php')">Satuan</a><br />
</div>

<div class="easyui-panel" title="TRANSAKSI" collapsed="true" collapsible="true" style="width:200px;height:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-cart" plain="true" onClick="addTab('Kontrak','order.php')">Order Data</a><br />
<a href="?module=pembelian" class="easyui-linkbutton" iconCls="icon-cart" plain="true" onclick="NewWindow('Order.php','Kontrak','960','600','yes');return false;">Order Data</a><br/>
<a href="?module=stpnb" class="easyui-linkbutton" iconCls="icon-truck" plain="true" onclick="NewWindow('stpnb.php','Kontrak','960','600','yes');return false;">Barang Masuk</a><br/>
<a href="#" class="easyui-linkbutton" iconCls="icon-truck" plain="true" onClick="addTab('Barang Keluar','barang_keluar.php')">Barang Keluar</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Mutasi Proses','mutasi_proses.php')">Mutasi Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Mutasi Hasil','mutasi_hasil.php')">Mutasi Hasil</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-stack-e" plain="true" onClick="addTab('Barang Dalam Proses','barang_proses.php')">Barang Dalam Proses</a>
</div>
<div class="easyui-panel" title="LAPORAN"  collapsed="true" collapsible="true"  style="width:200px;height:auto;padding:10px;" >
<a href="#" class="easyui-linkbutton" iconCls="icon-report1" plain="true" onClick="addTab('Laporan Data Order','laporan/data_order/pilih_lap.php')">Data Order</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Barang Masuk','laporan/barang_masuk/pilih_lap.php')">Barang Masuk</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Barang Keluar','laporan/barang_keluar/pilih_lap.php')">Barang Keluar</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Proses','laporan/mutasi_proses/pilih_lap.php')">Mutasi Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Hasil','laporan/mutasi_hasil/pilih_lap.php')">Mutasi Hasil</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Barang Dalam Proses','laporan/barang_proses/pilih_lap.php')">Barang Dalam Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report4" plain="true" onClick="addTab('Laporan Stok Barang','laporan/stok_barang/pilih_lap.php')">Stok Barang</a>
</div>
<div class="easyui-panel" title="AKSES" collapsible="true"  style="width:200px;height:auto;padding:10px;">
<a href="akses/logout.php" class="easyui-linkbutton" iconCls="icon-logout" plain="true">Logout</a>
</div>
</div>
</div>
<div id="isi">
  <div id="tt" class="easyui-tabs" style="height:500px;">
<div title="Home" style="padding-top:20px; text-align:center; background-image:url(mycss/images/materials.png); background-repeat:no-repeat;background-color:#FFF;">
</div>
</div>
</div>
<div id="footer">
 SBC.SWAG.2013.01.01
</div>
</body>
</html>