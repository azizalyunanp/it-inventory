<?php
include 'inc/cek_session.php';
include "inc/inc.koneksi.php";
include "inc/library.php";
include "inc/fungsi_indotgl.php";
include "inc/fungsi_combobox.php";
include "inc/class_paging.php";
include "inc/fungsi_rupiah.php";
include "inc/fungsi_tanggal.php";
include "inc/fungsi_hdt.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistem Informasi Emport-Import</title>
<link rel="stylesheet" type="text/css" href="mycss/index.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="jquery_easyui/themes/panel.css" />
<script type="text/javascript" src="jquery_easyui/jquery.min.js"></script>
<script type="text/javascript" src="jquery_easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="js/hoverIntent.js"></script>
<!-- untuk menu superfish -->
<script type="text/javascript" src="js/superfish.js"></script>

<!-- untuk datepicker -->
<link type="text/css" href="css/ui.all.css" rel="stylesheet" />   
<script type="text/javascript" src="js/ui.core.js"></script>
<script type="text/javascript" src="js/ui.datepicker.js"></script>
<script type="text/javascript" src="js/ui.datepicker-id.js"></script>

<!-- untuk autocomplite -->
<link rel="stylesheet" type="text/css" href="js/jquery.autocomplete.css" />
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	   $('ul.sf-menu').superfish();
  });
</script>
<script>
function addTab(title, url){
			if ($('#tt').tabs('exists', title)){
				$('#tt').tabs('select', title);
			} else {
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
				$('#tt').tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}
		}
	</script>
</head>

<body>
<div id="header">
</div>
<div id="navigasi">
<div style="width:200px;height:auto;padding:5px;float:left; margin-right:10px;background:#7190E0;">
<div class="easyui-panel" title="OLAH DATA MASTER" collapsed="true" collapsible="true" style="width:200px;height:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-report4" plain="true" onClick="addTab('Jenis Dokumen','jenis_dokumen.php')">Jenis Dokumen</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-supplier" plain="true" onClick="addTab('Data Buyer','buyer.php')">Data Buyer</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box" plain="true" onClick="addTab('Jenis Barang','jenis_barang.php')">Jenis Barang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-box-fill" plain="true" onClick="addTab('Data Barang','barang.php')">Data Barang</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Jenis Mutasi','jenis_mutasi.php')">Jenis Mutasi</a><br />
</div>
<br/>
<div class="easyui-panel" title="TRANSAKSI" collapsible="true" style="width:200px;height:auto;padding:10px;">
<a href="?module=pembelian" class="easyui-linkbutton" iconCls="icon-cart" plain="true" onClick="addTab('Data Order','data_master/pembelian/pembelian.php')">Data Order</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-truck" plain="true" onClick="addTab('Barang Masuk','barang_masuk.php')">Barang Masuk</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-truck" plain="true" onClick="addTab('Barang Keluar','barang_keluar.php')">Barang Keluar</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Mutasi Proses','mutasi_proses.php')">Mutasi Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-trolly" plain="true" onClick="addTab('Mutasi Hasil','mutasi_hasil.php')">Mutasi Hasil</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-stack-e" plain="true" onClick="addTab('Barang Dalam Proses','barang_proses.php')">Barang Dalam Proses</a>
</div><br />
<div class="easyui-panel" title="LAPORAN" collapsible="true"  style="width:200px;height:auto;padding:10px;">
<a href="#" class="easyui-linkbutton" iconCls="icon-report1" plain="true" onClick="addTab('Laporan Data Order','laporan/data_order/pilih_lap.php')">Data Order</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Barang Masuk','laporan/barang_masuk/pilih_lap.php')">Barang Masuk</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report2" plain="true" onClick="addTab('Laporan Barang Keluar','laporan/barang_keluar/pilih_lap.php')">Barang Keluar</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Proses','laporan/mutasi_proses/pilih_lap.php')">Mutasi Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Mutasi Hasil','laporan/mutasi_hasil/pilih_lap.php')">Mutasi Hasil</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report3" plain="true" onClick="addTab('Laporan Barang Dalam Proses','laporan/barang_proses/pilih_lap.php')">Barang Dalam Proses</a><br />
<a href="#" class="easyui-linkbutton" iconCls="icon-report4" plain="true" onClick="addTab('Laporan Stok Barang','laporan/stok_barang/pilih_lap.php')">Stok Barang</a>
</div>
<div class="easyui-panel" title="AKSES" collapsible="true"  style="width:200px;height:auto;padding:10px;">
<a href="akses/logout.php" class="easyui-linkbutton" iconCls="icon-logout" plain="true">Logout</a>
</div>
</div>
</div>
<div id="isi">
  <div id="tt" class="easyui-tabs" style="height:500px;">
<div title="Home" style="padding-top:20px; text-align:center; background-image:url(mycss/images/materials.png); background-repeat:no-repeat;background-color:#FFF;">
</div>
</div>
</div>
<!--awal content -->
    <div class="content">
    	<?php
			include 'content.php';
		?>
    </div>
<div id="footer">
 SBC.SWAG.2013.01.01
</div>
</body>
</html>
